
# Open Add account settings page (doesn't work if already opened and not closed)
adb shell am start -a android.settings.ADD_ACCOUNT_SETTINGS

sleep 1
# Select /e/ account 
# number of input depend on language set on the device

adb shell input keyevent TAB
sleep 1

# Open the /e/ account : add account form
adb shell input keyevent ENTER


# CREDENTIALS & SERVER URL

MAIL="PUT YOUR VALUE"
PWD="PUT YOUR VALUE"
SERVER_URI="PUT YOUR VALUE"

sleep 1
#Insert Login
adb shell input text $MAIL
adb shell input keyevent TAB

sleep 1
#Insert PWD
adb shell input text $PWD

## IF NO server URI 
if [ -z $SERVER_URI ]
then
	adb shell input keyevent TAB
	adb shell input keyevent ENTER
else

	# Move across element
	adb shell input keyevent TAB
	adb shell input keyevent TAB
	adb shell input keyevent TAB

	# Enter server URI
	adb shell input keyevent ENTER
	adb shell input keyevent TAB
	adb shell input text $SERVER_URI

	# Move again to login btn
	adb shell input keyevent TAB
	adb shell input keyevent TAB
	adb shell input keyevent TAB
	adb shell input keyevent TAB
	adb shell input keyevent TAB
	adb shell input keyevent ENTER
fi
