package foundation.e.drive;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.nextcloud.common.NextcloudClient;
import com.owncloud.android.lib.common.network.CertificateCombinedException;
import com.owncloud.android.lib.common.network.NetworkUtils;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.utils.Log_OC;
import com.owncloud.android.lib.resources.users.GetStatusRemoteOperation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;

import static com.owncloud.android.lib.common.accounts.AccountUtils.Constants.KEY_OC_BASE_URL;
import static org.junit.Assert.assertTrue;

import androidx.work.Configuration;
import androidx.work.testing.SynchronousExecutor;
import androidx.work.testing.WorkManagerTestInitHelper;

import okhttp3.CookieJar;

public abstract class TestUtils {
    public static final String TEST_LOCAL_ROOT_FOLDER_PATH = "/tmp/eDrive/test/"; //THis is where test file and folder for synchronisatio will be stored
    public static final String TEST_REMOTE_ROOT_FOLDER_PATH ="/eDrive-test/";
    public static String TEST_ACCOUNT_TYPE ="eelo";
    public static String TEST_SERVER_URI;
    public static String TEST_ACCOUNT_NAME;
    private static String TEST_ACCOUNT_PASSWORD;
    private static Account validAccount;


    /**
     * This method execute before the class, it assure that credentials are available
     */
    public static void loadServerCredentials() {
        final Properties properties = System.getProperties();
        TEST_ACCOUNT_PASSWORD = properties.getProperty("test.account.password");
        TEST_ACCOUNT_NAME = properties.getProperty("test.account.username");
        TEST_SERVER_URI = properties.getProperty("test.account.url");
    }

    /**
     * Get the valid Account object. Create it if it isn't already instanciated
     * @return Account
     */
    public static Account getValidAccount() {
        if (validAccount == null) {
            System.out.println("Account name = "+TEST_ACCOUNT_NAME);
            validAccount = new Account(TEST_ACCOUNT_NAME, TEST_ACCOUNT_TYPE);
        }
        return validAccount;
    }

    public static NextcloudClient getNcClient(Context context) {
        final Uri serverUri = Uri.parse(TEST_SERVER_URI);
        return new NextcloudClient(serverUri, TEST_ACCOUNT_NAME, TEST_ACCOUNT_PASSWORD, context, CookieJar.NO_COOKIES);
    }

    /**
     * register in accountManager an account with name, password, type and server url provided at build
     */
    public static void prepareValidAccount(AccountManager accountManager) {
        storeAccountInManager(getValidAccount(), TEST_ACCOUNT_PASSWORD, TEST_SERVER_URI, accountManager);
    }

    public static void storeAccountInManager(Account account, String password, String serverUrl, AccountManager manager) {
        final Bundle userData = new Bundle();
        userData.putString(KEY_OC_BASE_URL, serverUrl);
        manager.addAccountExplicitly(account, password, userData);
    }

    /**
     * Test the connexion to the server
     * Add the certificate to the knownServerCertificateStore if required
     * @throws KeyStoreException exception
     * @throws CertificateException exception
     * @throws NoSuchAlgorithmException exception
     * @throws IOException exception
     * @throws InterruptedException exception
     */
    public static void testConnection(final NextcloudClient client, final Context context) throws KeyStoreException,
            CertificateException,
            NoSuchAlgorithmException,
            IOException,
            InterruptedException{

        GetStatusRemoteOperation getStatus = new GetStatusRemoteOperation();

        RemoteOperationResult result = getStatus.execute(client);

        if (result.isSuccess()) {
            Log_OC.d("AbstractIT", "Connection to server successful");
        } else {
            if (RemoteOperationResult.ResultCode.SSL_RECOVERABLE_PEER_UNVERIFIED.equals(result.getCode())) {
                Log_OC.d("AbstractIT", "Accepting certificate");

                final CertificateCombinedException exception = (CertificateCombinedException) result.getException();
                final X509Certificate certificate = exception.getServerCertificate();

                NetworkUtils.addCertToKnownServersStore(certificate, context);
                Thread.sleep(1000);

                // retry
                getStatus = new GetStatusRemoteOperation();
                result = getStatus.execute(client);

                if (!result.isSuccess()) {
                    throw new RuntimeException("No connection to server possible, even with accepted cert");
                }
            } else {
                throw new RuntimeException("No connection to server possible");
            }
        }
    }

    /**
     * Method adapted from <a href="https://github.com/nextcloud/android/blob/master/src/androidTest/java/com/owncloud/android/AbstractIT.java">...</a>
     * @param filePath path of the file to create
     * @param iteration number of time to write dummy content
     * @return the File instance
     * @throws IOException exception
     * @throws SecurityException exception
     */
    public static File createFile(String filePath, int iteration) throws IOException, SecurityException{
        final File file = new File(filePath);
        if (file.getParentFile() != null && !file.getParentFile().exists()) {
            assertTrue(file.getParentFile().mkdirs());
        }

        file.createNewFile();

        final FileWriter writer = new FileWriter(file);
        for (int i = 0; i < iteration; i++) {
            writer.write("123123123123123123123123123\n");
        }
        writer.flush();
        writer.close();

        return file;
    }

    /**
     * Delete a local directory with all its content.
     * Adapted from <a href="https://www.geeksforgeeks.org/java-program-to-delete-a-directory/">...</a>
     * @param file
     */
    public static void deleteDirectory(File file)
    {
        for (File subfile : file.listFiles()) {
            if (subfile.isDirectory()) {
                deleteDirectory(subfile);
            }
            subfile.delete();
        }
        file.delete();
    }

    public static void initializeWorkmanager(Context context) {
        final Configuration config = new Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(new SynchronousExecutor())
                .build();
        WorkManagerTestInitHelper.initializeTestWorkManager(
                context, config);
    }
}