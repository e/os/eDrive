/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan.contentScanner;


import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.TestUtils;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.periodicScan.contentScanner.FolderWrapper;
import foundation.e.drive.periodicScan.contentScanner.LocalFileLister;

/**
 * @author vincent Bourgmayer
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class LocalFileListerTest {

    private static final String TEST_FILE_TREE_ROOT_PATH = "../tmp/";

    private LocalFileLister fileListerUnderTest;
    private final Context context;
    private final File testRootDirectory;

    public LocalFileListerTest() {
        context = ApplicationProvider.getApplicationContext();
        ShadowLog.stream = System.out;
        testRootDirectory = new File(TEST_FILE_TREE_ROOT_PATH);
    }

    @Before
    public void setUp() {
        fileListerUnderTest = new LocalFileLister(null);
    }


    @After
    public void tearDown() {
        SQLiteDatabase.deleteDatabase(context.getDatabasePath(DbHelper.DATABASE_NAME));

        if (testRootDirectory.exists()) {
            TestUtils.deleteDirectory(testRootDirectory);
        }
    }

    @Test
    public void skipSyncedFolder_notMedia_notHidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, false, false);
        Assert.assertTrue(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_notMedia_notHidden_scannable_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, false, true);
        Assert.assertFalse(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_notMedia_hidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, true, false);
        Assert.assertTrue(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_notMedia_hidden_scannable_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, true, true);
        Assert.assertFalse(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_media_notHidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, false, false);
        Assert.assertTrue(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_media_notHidden_scannable_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, false, true);
        Assert.assertFalse("folder shouldn't be skip but it had", fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_media_Hidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, true, false);
        Assert.assertTrue(fileListerUnderTest.skipSyncedFolder(folder));
    }

    @Test
    public void skipSyncedFolder_media_Hidden_scannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, true, true);
        Assert.assertTrue(fileListerUnderTest.skipSyncedFolder(folder));
    }

    private SyncedFolder buildSyncedFolderForSkipSyncedFolderTest(boolean isMedia, boolean isHidden, boolean isLocalScannable) {
        final String dirName = isHidden ? ".myFolder/" : "myFolder/";
        return new SyncedFolder("any", TEST_FILE_TREE_ROOT_PATH + dirName, "/remote/path/" + dirName, isLocalScannable, true, true, isMedia);
    }

    @Test
    public void skipDirectory_lastModifiedSame_noUnsyncedContent_shouldReturnTrue() {
        final long lastModified = 123456L;

        final File directory = Mockito.spy(testRootDirectory);
        Mockito.when(directory.lastModified()).thenReturn(lastModified);

        final SyncedFolder syncedFolder = new SyncedFolder("any", directory.getAbsolutePath(), "/remote/path/test/", true, true, true, true);
        syncedFolder.setId(12);
        syncedFolder.setLastModified(lastModified);

        final boolean skip = fileListerUnderTest.skipDirectory(directory, syncedFolder, context);
        Assert.assertTrue("directory without new last modified and no content waiting for sync has not been skip", skip);
    }

    @Test
    public void skipDirectory_lastModifiedSame_unsyncedContent_shouldReturnFalse() {
        final long lastModified = 123456L;

        final String dirPath = testRootDirectory.getAbsolutePath();

        final File directory = Mockito.spy(testRootDirectory);
        Mockito.when(directory.lastModified()).thenReturn(lastModified);

        final SyncedFileState dummy = new SyncedFileState(6, "foo.jpg", dirPath + "foo.jpg", "/remote/path/test/foo.jpg", "", 0L, 12, true, SCAN_EVERYWHERE);
        DbHelper.manageSyncedFileStateDB(dummy, "INSERT", context);

        final SyncedFolder syncedFolder = new SyncedFolder("any", dirPath , "/remote/path/test/", true, true, true, true);
        syncedFolder.setId(12);
        syncedFolder.setLastModified(lastModified);

        final boolean skip = fileListerUnderTest.skipDirectory(directory, syncedFolder, context);
        Assert.assertFalse("directory without new last modified but content waiting for sync has been skip", skip);
    }

    @Test
    public void skipDirectory_lastModifiedChanged_noUnsyncedContent_shouldReturnFalse() {
        final String dirPath = testRootDirectory.getAbsolutePath();

        final File directory = Mockito.spy(testRootDirectory);
        Mockito.when(directory.lastModified()).thenReturn(654321L);

        final SyncedFolder syncedFolder = new SyncedFolder("any", dirPath, "/remote/path/test/", true, true, true, true);
        syncedFolder.setId(12);
        syncedFolder.setLastModified(123456L);

        final boolean skip = fileListerUnderTest.skipDirectory(directory, syncedFolder, context);
        Assert.assertFalse("directory with new last modified and no content waiting for sync has been skip", skip);
    }

    @Test
    public void skipDirectory_lastModifiedChanged_unsyncedContent_shouldReturnFalse() {
        final String dirPath = testRootDirectory.getAbsolutePath();

        final File directory = Mockito.spy(testRootDirectory);
        Mockito.when(directory.lastModified()).thenReturn(654321L);

        final SyncedFolder syncedFolder = new SyncedFolder("any", dirPath, "/remote/path/test/", true, true, true, true);
        syncedFolder.setId(12);
        syncedFolder.setLastModified(123456L);

        final SyncedFileState dummy = new SyncedFileState(6, "foo.jpg", dirPath + "foo.jpg", "/remote/path/test/foo.jpg", "", 0L, 12, true, SCAN_EVERYWHERE);
        DbHelper.manageSyncedFileStateDB(dummy, "INSERT", context);

        final boolean skip = fileListerUnderTest.skipDirectory(directory, syncedFolder, context);
        Assert.assertFalse("directory with new last modified and content waiting for sync has been skip", skip);
    }

    //Test about inner class: DirectoryLoader
    @Test
    public void loadDirectory_dirNotExist_shouldReturnTrue() {
        final String dirPath = testRootDirectory.getAbsolutePath();

        final SyncedFolder folder = new SyncedFolder("any", dirPath, "/remote/path/test", true);

        final LocalFileLister.LocalFolderLoader folderLoader = fileListerUnderTest.createFolderLoader();

        final boolean loadSuccess = folderLoader.load(folder);
        Assert.assertTrue("Loading remote folder resulting in 404 should return true", loadSuccess);
        Assert.assertTrue(folderLoader.getFolderWrapper().isMissing());
    }

    @Test
    public void loadDirectory_dirExist_noContent_shouldReturnTrue() {
        final String dirPath = testRootDirectory.getAbsolutePath();

        final SyncedFolder folder = new SyncedFolder("any", dirPath, "/remote/path/test/", true);

        final boolean dirCreationSucceed = testRootDirectory.mkdirs();

        Assert.assertTrue("Cannot create test folder: " + dirPath, dirCreationSucceed);

        final LocalFileLister.LocalFolderLoader folderLoader = fileListerUnderTest.createFolderLoader();

        final boolean loadSuccess = folderLoader.load(folder);
        final FolderWrapper<File> wrapper = folderLoader.getFolderWrapper();
        Assert.assertTrue("Loading remote folder resulting in 404 should return true", loadSuccess);
        Assert.assertFalse("dir shouldExist!", wrapper.isMissing());
        Assert.assertTrue("List of folderWrapper's content expected to be empty but contains: " + wrapper.getContent().size(), wrapper.getContent().isEmpty());
    }

    @Test
    public void loadDirectory_dirExist_withSubContent_shouldReturnTrue() {
        final String dirPath = testRootDirectory.getAbsolutePath();

        final SyncedFolder folder = new SyncedFolder("any", dirPath, "/remote/path/test/", true);
        final boolean dirCreationSucceed = testRootDirectory.mkdirs();
        Assert.assertTrue("cannot create test folder: " + dirPath, dirCreationSucceed);


        final File subContent = new File(testRootDirectory, "foo.txt");
        try {
            Assert.assertTrue("Can't create subFile", subContent.createNewFile());
        } catch (IOException exception) {
            exception.printStackTrace();
            Assert.fail("IO Exception: Can't create new file as content of test folder");
        }

        final LocalFileLister.LocalFolderLoader folderLoader = fileListerUnderTest.createFolderLoader();

        final boolean loadSuccess = folderLoader.load(folder);
        final FolderWrapper<File> wrapper = folderLoader.getFolderWrapper();
        Assert.assertTrue("Loading remote folder resulting in 404 should return true", loadSuccess);
        Assert.assertFalse("dir shouldExist!", wrapper.isMissing());
        Assert.assertEquals("List of folderWrapper's content size expected to one but was: " + wrapper.getContent().size(), 1, wrapper.getContent().size());
    }


    /**
     * Run on a more realistic files tree to check the whole behaviour with more than
     * one directory:
     *
     *     It should contains :
     *     - one directory with a subdirectory with some files in both
     *             - one hidden directory with some files
     *             - one empty directory
     *     - one "new" directory with subfiles
     *
     *     with correspond syncedFolder in database:
     *     - except for one, so it will considered as new
     *             Something like:
     *
     *     ## File tree
     *     >Folder 1 (unChanged)
     *             --> .File 1 (should be ignored)
     *             --> File 2 (should be ignored)
     *             --> Folder 2 (unsynced dir)
     *             ----> File 3 (new file should be added)
     *             ----> .File 4 (should be ignored)
     *   >Folder 3 (unChanged)
     *             -->Folder4 (new dir (so unsynced)
     *             ----> File 5 (new file should be added)
     *             -->.Folder5 (skip: hidden dir)
     *             ----> File 6 (skip because directory is hidden)
     *
     *
     *     ## SyncedFolder
     *     SyncedFolder 1
     *     SyncedFolder 2 with empty lastModified
     *     SyncedFolder 3
     *     SyncedFolderRemoved which correspond to a missing directory
     */
    @Test
    public void test_listContentToScan() {
        Assert.assertTrue("Preparation, of file Tree and syncedFolder has failed", prepareFakeContent());
        Assert.assertTrue("File listing failed", fileListerUnderTest.listContentToScan(context));

        final List<Long> foldersId = fileListerUnderTest.getSyncedFoldersId();
        Assert.assertEquals("List of folders to scan's id should contains 3 but contained: " + foldersId.size(), 3, foldersId.size());
        Assert.assertTrue(foldersId.contains(2L)); //id of changed folder detected
        Assert.assertTrue(foldersId.contains(5L)); //id of new folder detected
        Assert.assertTrue(foldersId.contains(4L)); //id of Removed Folder detected
        final List<File> contentListed = fileListerUnderTest.getContentToScan();
        Assert.assertEquals("ContentListed should have 2 files but contained: " + contentListed.size(), 2, contentListed.size());
    }

    private boolean prepareFakeContent() {
        /*
         * Generate folders
         */
        if (!testRootDirectory.mkdirs()) return false;

        final File folder1 = new File(testRootDirectory, "folder1");
        if (!folder1.mkdirs()) return false;
        final File folder2 = new File(folder1, "folder2");
        if (!folder2.mkdirs()) return false;

        final File folder3 = new File(testRootDirectory, "folder3");
        if (!folder3.mkdirs()) return false;
        final File folder4 = new File(folder3, "folder4");
        if (!folder4.mkdirs()) return false;
        final File folder5 = new File(folder3, ".folder5");
        if (!folder5.mkdirs()) return false;


        /*
         * Generate files
         * /!\ must be done before syncedFolders generation
         * otherwise lastModified value of directory will change due to file creation
         */
        try {
            final File file1 = new File(folder1, ".file1.txt");
            if (!file1.createNewFile()) return false;

            final File file2 = new File(folder1, "file2.txt");
            if (!file2.createNewFile()) return false;

            final File file3 = new File(folder2, "file3.txt");
            if (!file3.createNewFile()) return false;

            final File file4 = new File(folder2, ".file4.txt");
            if (!file4.createNewFile()) return false;

            final File file5 = new File(folder4, "file5.txt");
            if (!file5.createNewFile()) return false;

            final File file6 = new File(folder5, "file6.txt");
            if (!file6.createNewFile()) return false;
        } catch (IOException exception) {
            exception.printStackTrace();
            return false;
        }

        /*
         * Generate SyncedFolders
         */
        final SyncedFolder syncedFolder1 = new SyncedFolder("any", folder1.getAbsolutePath() + "/", "/remote/path/myFolder1/", true);
        syncedFolder1.setLastModified(folder1.lastModified());
        syncedFolder1.setId((int) DbHelper.insertSyncedFolder(syncedFolder1, context));
        final SyncedFolder syncedFolder2 = new SyncedFolder("any", folder2.getAbsolutePath() + "/", "/remote/path/myFolder2/", true);
        syncedFolder2.setLastModified(0L);
        syncedFolder2.setId((int) DbHelper.insertSyncedFolder(syncedFolder2, context));
        final SyncedFolder syncedFolder3 = new SyncedFolder("any", folder3.getAbsolutePath() + "/", "/remote/path/myFolder3/", true);
        syncedFolder3.setLastModified(folder3.lastModified());
        syncedFolder3.setId((int) DbHelper.insertSyncedFolder(syncedFolder3, context));
        final SyncedFolder syncedFolderRemoved = new SyncedFolder("any", testRootDirectory.getAbsolutePath() + "/lambda/", "/remote/lambda", true);
        syncedFolderRemoved.setLastModified(987654321L);
        syncedFolderRemoved.setLastEtag("123456789");
        syncedFolderRemoved.setId((int) DbHelper.insertSyncedFolder(syncedFolderRemoved, context));

        final ArrayList<SyncedFolder> syncedFolders = new ArrayList<>();
        syncedFolders.add(syncedFolder1);
        syncedFolders.add(syncedFolder2);
        syncedFolders.add(syncedFolder3);
        syncedFolders.add(syncedFolderRemoved);
        fileListerUnderTest = new LocalFileLister(syncedFolders);

        return true;
    }
}
