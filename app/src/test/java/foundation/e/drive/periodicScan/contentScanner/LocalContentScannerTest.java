/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan.contentScanner;

import static org.mockito.Mockito.when;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.content.Context;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.periodicScan.contentScanner.LocalContentScanner;

/**
 * @author vincent Bourgmayer
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class LocalContentScannerTest {

    private final Context context;
    private LocalContentScanner scannerUnderTest;

    public LocalContentScannerTest() {
        context = ApplicationProvider.getApplicationContext();
        ShadowLog.stream = System.out;
    }

    @Before
    public void setUp() {
        final File folder = new File("local/path/");
        final List<SyncedFolder> directoryList = new ArrayList<>();
        // Without the + "/" for local path of SyncedFolder, some test will fails because folder.getAbsolutePath() doesn't have "/" at the end
        //but syncedFolders in database (in production) has the path separator at the end of the local path
        directoryList.add(new SyncedFolder("Photos", folder.getAbsolutePath() + "/", "remote/path/", true, true, true, true));
        scannerUnderTest = new LocalContentScanner(context, directoryList);
    }

    @Test
    public void scanContent_withMissingLocalFiles_returnListWithRemoteDeletionRequest() {
        final List<File> localFiles = new ArrayList<>();

        final List<SyncedFileState> syncedFileStates = new ArrayList<>();
        final SyncedFileState syncedFileState1 = new SyncedFileState(1, "titi.png", "local/path/titi.png", "remote/path/titi.png", "aaaa", 123456, 0, true, SCAN_EVERYWHERE);
        final SyncedFileState syncedFileState2 = new SyncedFileState(2, "tutu.png", "local/path/tutu.png", "remote/path/tutu.png", "bbbb", 234567, 0, true, SCAN_EVERYWHERE);

        syncedFileStates.add(syncedFileState1);
        syncedFileStates.add(syncedFileState2);

        final HashMap<Integer, SyncRequest> result = scannerUnderTest.scanContent( localFiles, syncedFileStates);
        Assert.assertEquals("Result is expected to be a list with 2 UploadRequest but has : " + result.size() + "elements", 2, result.size());
        Assert.assertTrue("Each SyncRequests should be upload request but some were not", result.values().stream().allMatch(request -> request.getOperationType().equals(SyncRequest.Type.DISABLE_SYNCING)));
    }

    @Test
    public void scanContent_withLocalFilesUnsynced_returnListWithUploadRequests() {
        final List<File> localFiles = new ArrayList<>();
        final File file1 = Mockito.spy(new File("local/path/titi.png"));
        when(file1.lastModified()).thenReturn(987654L);
        final File file2 = Mockito.spy(new File("local/path/tutu.png"));
        when(file2.lastModified()).thenReturn(654321L);

        localFiles.add(file1);
        localFiles.add(file2);

        final List<SyncedFileState> syncedFileStates = new ArrayList<>();
        final SyncedFileState syncedFileState1 = new SyncedFileState(1, file1.getName(), file1.getAbsolutePath(), "remote/path/titi.png", "", 0, 0, true, SCAN_EVERYWHERE);
        final SyncedFileState syncedFileState2 = new SyncedFileState(2, file2.getName(), file2.getAbsolutePath(), "remote/path/tutu.png", "", 0, 0, true, SCAN_EVERYWHERE);

        syncedFileStates.add(syncedFileState1);
        syncedFileStates.add(syncedFileState2);

        final HashMap<Integer, SyncRequest> result = scannerUnderTest.scanContent( localFiles, syncedFileStates);
        Assert.assertEquals("Result is expected to be a list with 2 UploadRequest but has : " + result.size() + "elements", 2, result.size());
        Assert.assertTrue("Each SyncRequests should be upload request but some were not", result.values().stream().allMatch(request -> request.getOperationType().equals(SyncRequest.Type.UPLOAD)));
    }


    @Test
    public void scanContent_withUpdatedLocalFiles_returnListWithUploadRequests() {
        final List<File> localFiles = new ArrayList<>();
        final File file1 = Mockito.spy(new File("local/path/titi.png"));
        when(file1.lastModified()).thenReturn(987654L);
        final File file2 = Mockito.spy(new File("local/path/tutu.png"));
        when(file2.lastModified()).thenReturn(654321L);

        localFiles.add(file1);
        localFiles.add(file2);

        final List<SyncedFileState> syncedFileStates = new ArrayList<>();
        final SyncedFileState syncedFileState1 = new SyncedFileState(1, file1.getName(), file1.getAbsolutePath(), "remote/path/titi.png", "aaaa", 123456, 0, true, SCAN_EVERYWHERE);
        final SyncedFileState syncedFileState2 = new SyncedFileState(2, file2.getName(), file2.getAbsolutePath(), "remote/path/tutu.png", "bbbb", 234567, 0, true, SCAN_EVERYWHERE);

        syncedFileStates.add(syncedFileState1);
        syncedFileStates.add(syncedFileState2);

        final HashMap<Integer, SyncRequest> result = scannerUnderTest.scanContent( localFiles, syncedFileStates);
        Assert.assertEquals("Result is expected to be a list with 2 UploadRequest but has : " + result.size() + "elements", 2, result.size());
        Assert.assertTrue("Each SyncRequests should be upload request but some were not", result.values().stream().allMatch(request -> request.getOperationType().equals(SyncRequest.Type.UPLOAD)));
    }

    @Test
    public void scanContent_withNewLocalFiles_returnListWithUploadRequests() {
        final List<File> localFiles = new ArrayList<>();
        final File newFile1 = new File("local/path/titi.png");
        final File newFile2 = new File("local/path/tutu.png");
        localFiles.add(newFile1);
        localFiles.add(newFile2);

        final List<SyncedFileState> syncedFileStates = new ArrayList<>();
        final HashMap<Integer, SyncRequest> result = scannerUnderTest.scanContent( localFiles, syncedFileStates);
        Assert.assertEquals("Result is expected to be a list with 2 UploadRequest but has : " + result.size() + "elements", 2, result.size());
        Assert.assertTrue("Each SyncRequests should be upload request but some were not", result.values().stream().allMatch(request -> request.getOperationType().equals(SyncRequest.Type.UPLOAD)));
    }

    @Test
    public void scanContent_emptyLists_returnEmptyList() {
        final List<File> localFiles = new ArrayList<>();
        final List<SyncedFileState> syncedFileStates = new ArrayList<>();
        final HashMap<Integer, SyncRequest> result = scannerUnderTest.scanContent( localFiles, syncedFileStates);
        Assert.assertTrue("Result is expected to be an empty list but contained: " + result.size(), result.isEmpty());
    }
}
