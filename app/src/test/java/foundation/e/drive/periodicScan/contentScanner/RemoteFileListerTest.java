/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan.contentScanner;

import static org.mockito.Mockito.when;

import static foundation.e.drive.models.SyncedFileStateKt.SCAN_EVERYWHERE;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import androidx.test.core.app.ApplicationProvider;

import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.model.RemoteFile;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.TestUtils;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.periodicScan.contentScanner.RemoteFileLister;
import foundation.e.drive.utils.DavClientProvider;

/**
 * @author vincent Bourgmayer
 */
@RunWith(RobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.O, manifest = Config.NONE)
public class RemoteFileListerTest {
    private static final String DIR_NO_SKIP_EXPECTED = "directory shouldn't have been skipped";
    private static final String DIR_SKIP_EXPECTED = "directory should have been skipped";
    private static final String TEST_FILE_TREE_ROOT_PATH = "/remote/";

    private RemoteFileLister fileListerUnderTest;
    private final Account account;
    private final OwnCloudClient ocClient;
    private final Context context;

    public RemoteFileListerTest() {
        context = ApplicationProvider.getApplicationContext();
        ShadowLog.stream = System.out;
        TestUtils.loadServerCredentials();
        TestUtils.prepareValidAccount(AccountManager.get(context));
        account = TestUtils.getValidAccount();
        ocClient = DavClientProvider.getInstance().getClientInstance(account, context);
    }

    @Before
    public void setUp() {
        fileListerUnderTest = new RemoteFileLister(null, ocClient);
        SQLiteDatabase.deleteDatabase(context.getDatabasePath(DbHelper.DATABASE_NAME));
    }

    @Test
    public void skipSyncedFolder_notMedia_notHidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, false, false);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertTrue("Not media, not hidden, not remotly scannable " + DIR_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_notMedia_notHidden_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, false, true);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertFalse("Not media, not hidden, remotly scannable " + DIR_NO_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_notMedia_hidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, true, false);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertTrue("Not media, hidden, not remotly scannable " + DIR_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_notMedia_hidden_scannable_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(false, true, true);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertFalse("Not media, hidden, remotly scannable " + DIR_NO_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_media_notHidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, false, false);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertTrue("media, not hidden, not remotly scannable " + DIR_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_media_notHidden_scannable_shouldReturnFalse() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, false, true);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertFalse("media, not hidden, remotly scannable " + DIR_NO_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_media_hidden_notScannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, true, false);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertTrue("media, hidden, not remotly scannable " + DIR_SKIP_EXPECTED, skipFolder);
    }

    @Test
    public void skipSyncedFolder_media_hidden_scannable_shouldReturnTrue() {
        final SyncedFolder folder = buildSyncedFolderForSkipSyncedFolderTest(true, true, true);
        final boolean skipFolder = fileListerUnderTest.skipSyncedFolder(folder);
        Assert.assertTrue("media, hidden, remotly scannable " + DIR_SKIP_EXPECTED, skipFolder);
    }

    private SyncedFolder buildSyncedFolderForSkipSyncedFolderTest(boolean isMedia, boolean isHidden, boolean isRemoteScannable) {
        final String dirName = isHidden ? ".myFolder/" : "myFolder/";
        return new SyncedFolder("any", "/local/path/" + dirName, TEST_FILE_TREE_ROOT_PATH + dirName, true, isRemoteScannable, true, isMedia);
    }


    @Test
    public void skipFile_hidden_shouldReturnTrue() {
        final String hiddenRemoteFilePath = TEST_FILE_TREE_ROOT_PATH + ".foo.png";
        final RemoteFile file = Mockito.mock(RemoteFile.class);
        when(file.getRemotePath()).thenReturn(hiddenRemoteFilePath);

        final boolean skipFile = fileListerUnderTest.skipFile(file);
        Assert.assertTrue("Hidden remoteFile (" + hiddenRemoteFilePath + ") should have been skipped", skipFile);
    }

    @Test
    public void skipFile_notHidden_shouldReturnFalse() {
        final String notHiddenRemoteFilePath = TEST_FILE_TREE_ROOT_PATH + "foo.png";
        final RemoteFile file = Mockito.mock(RemoteFile.class);
        when(file.getRemotePath()).thenReturn(notHiddenRemoteFilePath);

        final boolean skipFile = fileListerUnderTest.skipFile(file);
        Assert.assertFalse("Not hidden remoteFile (" + notHiddenRemoteFilePath + ") should not have been skipped", skipFile);
    }

    @Test
    public void skipFile_emptyPath_shouldReturnTrue() {
        final String emptyPath = "";
        final RemoteFile file = Mockito.mock(RemoteFile.class);
        when(file.getRemotePath()).thenReturn(emptyPath);

        final boolean skipFile = fileListerUnderTest.skipFile(file);
        Assert.assertTrue("RemoteFile with empty path (" + emptyPath + ") should have been skipped", skipFile);
    }

    @Test
    @Ignore("will fail")
    public void skipFile_invalidPath_shouldReturnTrue() {
        final String invalidPath = "/invalid/path/";
        /*in this case: No file, just a directory
        the current implementation of CommonUtils.getFileName(String path)
        will return "path" while empty string should be expected.
        todo: fix CommonUtils.getFileName(String path) */
        final RemoteFile file = Mockito.mock(RemoteFile.class);
        when(file.getRemotePath()).thenReturn(invalidPath);

        final boolean skipFile = fileListerUnderTest.skipFile(file);
        Assert.assertTrue("RemoteFile with empty path (" + invalidPath + ") should have been skipped", skipFile);
    }

    @Test
    public void skipDirectory_etagChanged_noUnsyncedContent_shouldReturnFalse() {
        final RemoteFile remoteFile = Mockito.mock(RemoteFile.class);
        when(remoteFile.getEtag()).thenReturn("6666bbbb");

        final SyncedFolder syncedFolder = new SyncedFolder("", "local", TEST_FILE_TREE_ROOT_PATH, true);
        syncedFolder.setId(12);
        syncedFolder.setLastEtag("5555aaaa");

        final boolean skip = fileListerUnderTest.skipDirectory(remoteFile, syncedFolder, context);

        Assert.assertFalse("Remote directory with new etag should not have been skipped but it had", skip);
    }

    @Test
    public void skipDirectory_etagSame_noUnsyncedContent_shouldReturnTrue() {
        final RemoteFile remoteFile = Mockito.mock(RemoteFile.class);
        when(remoteFile.getEtag()).thenReturn("5555aaaa");

        final SyncedFolder syncedFolder = new SyncedFolder("", "local", TEST_FILE_TREE_ROOT_PATH, true);
        syncedFolder.setId(12);
        syncedFolder.setLastEtag("5555aaaa");

        final boolean skip = fileListerUnderTest.skipDirectory(remoteFile, syncedFolder, context);

        Assert.assertTrue("Remote directory with no new etag should have been skipped but had not", skip);
    }

    @Test
    public void skipDirectory_etagSame_contentToSync_shouldReturnFalse() {
        final RemoteFile remoteFile = Mockito.mock(RemoteFile.class);
        when(remoteFile.getEtag()).thenReturn("5555aaaa");

        final SyncedFolder syncedFolder = new SyncedFolder("", "local", TEST_FILE_TREE_ROOT_PATH, true);
        syncedFolder.setId(12);
        syncedFolder.setLastEtag("5555aaaa");

        final SyncedFileState dummy = new SyncedFileState(6, "foo.jpg", "local/foo.jpg", TEST_FILE_TREE_ROOT_PATH + "foo.jpg", "7777", 0L, 12, true, SCAN_EVERYWHERE);
        DbHelper.manageSyncedFileStateDB(dummy, "INSERT", context);

        final boolean skip = fileListerUnderTest.skipDirectory(remoteFile, syncedFolder, context);
        Assert.assertFalse("Remote directory with no new etag but there is content to sync from DB: should not have been skipped but it had", skip);
    }

    @Test
    public void skipDirectory_etagChanged_contentToSync_shouldReturnFalse() {
        final RemoteFile remoteFile = Mockito.mock(RemoteFile.class);
        when(remoteFile.getEtag()).thenReturn("6666bbbb");

        final SyncedFolder syncedFolder = new SyncedFolder("", "local", TEST_FILE_TREE_ROOT_PATH, true);
        syncedFolder.setId(12);
        syncedFolder.setLastEtag("5555aaaa");

        final SyncedFileState dummy = new SyncedFileState(6, "foo.jpg", "local/foo.jpg", TEST_FILE_TREE_ROOT_PATH + "foo.jpg", "7777", 0L, 12, true, SCAN_EVERYWHERE);
        DbHelper.manageSyncedFileStateDB(dummy, "INSERT", context);

        final boolean skip = fileListerUnderTest.skipDirectory(remoteFile, syncedFolder, context);
        Assert.assertFalse("Remote directory with new etag and there is content to sync from DB: should not have been skipped but it had", skip);
    }

    //Test about inner class: DirectoryLoader
    @Test
    public void loadDirectory_serverResponse404_shouldReturnTrue() {
        final String remotePath = TEST_FILE_TREE_ROOT_PATH + "myFolder/";
        final SyncedFolder folder = new SyncedFolder("any", "local/myFolder", remotePath, true);

        final RemoteOperationResult fakeResult = Mockito.mock(RemoteOperationResult.class);
        when(fakeResult.getHttpCode()).thenReturn(404);
        when(fakeResult.isSuccess()).thenReturn(false);

        final RemoteFileLister.RemoteFolderLoader mockLoaderSpied = Mockito.spy(fileListerUnderTest.createFolderLoader());
        when(mockLoaderSpied.readRemoteFolder(remotePath, ocClient)).thenReturn(fakeResult);

        final boolean loadSuccess = mockLoaderSpied.load(folder);
        Assert.assertTrue("Loading remote folder resulting in 404 should return true", loadSuccess);
    }

    @Test
    public void loadDirectory_serverResponse405_shouldReturnFalse() {
        final String remotePath = TEST_FILE_TREE_ROOT_PATH + "myFolder/";
        final SyncedFolder folder = new SyncedFolder("any", "local/myFolder", remotePath, true);

        final RemoteOperationResult fakeResult = Mockito.mock(RemoteOperationResult.class);
        when(fakeResult.getHttpCode()).thenReturn(405);
        when(fakeResult.isSuccess()).thenReturn(false);

        final RemoteFileLister.RemoteFolderLoader mockLoaderSpied = Mockito.spy(fileListerUnderTest.createFolderLoader());
        when(mockLoaderSpied.readRemoteFolder(remotePath, ocClient)).thenReturn(fakeResult);

        final boolean loadSuccess = mockLoaderSpied.load(folder);
        Assert.assertFalse("Loading remote folder resulting in 405 should return false", loadSuccess);
    }

    @Test
    public void loadDirectory_successAndErrorCode_shouldReturnTrue() {
        final String remotePath = TEST_FILE_TREE_ROOT_PATH + "myFolder/";
        final SyncedFolder folder = new SyncedFolder("any", "local/myFolder", remotePath, true);

        final RemoteOperationResult fakeResult = Mockito.mock(RemoteOperationResult.class);
        when(fakeResult.getHttpCode()).thenReturn(405);
        when(fakeResult.isSuccess()).thenReturn(true);

        final ArrayList<RemoteFile> fakeData = new ArrayList<>();
        final RemoteFile mockRemoteFile = Mockito.mock(RemoteFile.class);
        fakeData.add(mockRemoteFile);
        when(fakeResult.getData()).thenReturn(fakeData);

        final RemoteFileLister.RemoteFolderLoader spiedLoaderUnderTest = Mockito.spy(fileListerUnderTest.createFolderLoader());
        when(spiedLoaderUnderTest.readRemoteFolder(remotePath, ocClient)).thenReturn(fakeResult);

        final boolean loadSuccess = spiedLoaderUnderTest.load(folder);
        Assert.assertTrue("Loading remote folder with success despite error code (405) should return true", loadSuccess);
        Assert.assertEquals("Loaded Folder is not the one expected despite loading success", mockRemoteFile, spiedLoaderUnderTest.getFolderWrapper().getFolder());
    }

    @Test
    public void loadDirectory_successAndRemoteContent_shouldReturnTrue() {
        final String remotePath = TEST_FILE_TREE_ROOT_PATH + "myFolder/";
        final SyncedFolder folder = new SyncedFolder("any", "local/myFolder", remotePath, true);

        final RemoteOperationResult fakeResult = Mockito.mock(RemoteOperationResult.class);
        when(fakeResult.getHttpCode()).thenReturn(405);
        when(fakeResult.isSuccess()).thenReturn(true);

        final ArrayList<RemoteFile> fakeData = new ArrayList<>();
        final RemoteFile mockDirRemoteFile = Mockito.mock(RemoteFile.class);
        fakeData.add(mockDirRemoteFile);
        final RemoteFile mockContentRemoteFile1 = Mockito.mock(RemoteFile.class);
        final RemoteFile mockContentRemoteFile2 = Mockito.mock(RemoteFile.class);
        fakeData.add(mockContentRemoteFile1);
        fakeData.add(mockContentRemoteFile2);
        when(fakeResult.getData()).thenReturn(fakeData);

        final RemoteFileLister.RemoteFolderLoader spiedLoaderUnderTest = Mockito.spy(fileListerUnderTest.createFolderLoader());
        when(spiedLoaderUnderTest.readRemoteFolder(remotePath, ocClient)).thenReturn(fakeResult);

        final boolean loadSuccess = spiedLoaderUnderTest.load(folder);
        Assert.assertTrue("Loading remote folder with success despite error code (405) should return true", loadSuccess);
        Assert.assertEquals("Loaded Folder is not the one expected despite loading success", mockDirRemoteFile, spiedLoaderUnderTest.getFolderWrapper().getFolder());
        Assert.assertEquals("mockDirRemoteFile should have been the directory loader but is not", 2, spiedLoaderUnderTest.getFolderWrapper().getContent().size());
        Assert.assertTrue("mockContentRemoteFile1 should have been in the result of loading but is not", spiedLoaderUnderTest.getFolderWrapper().getContent().contains(mockContentRemoteFile1));
        Assert.assertTrue("mockContentRemoteFile2 should have been in the result of loading but is not", spiedLoaderUnderTest.getFolderWrapper().getContent().contains(mockContentRemoteFile2));
    }

    @Test
    public void loadDirectory_successButNoData_shouldReturnFalse() {
        final String remotePath = TEST_FILE_TREE_ROOT_PATH + "myFolder/";
        final SyncedFolder folder = new SyncedFolder("any", "local/myFolder", remotePath, true);

        final RemoteOperationResult fakeResult = Mockito.mock(RemoteOperationResult.class);
        when(fakeResult.getHttpCode()).thenReturn(204);
        when(fakeResult.isSuccess()).thenReturn(true);

        final ArrayList<RemoteFile> fakeData = new ArrayList<>();
        when(fakeResult.getData()).thenReturn(fakeData);

        final RemoteFileLister.RemoteFolderLoader mockLoaderSpied = Mockito.spy(fileListerUnderTest.createFolderLoader());
        when(mockLoaderSpied.readRemoteFolder(remotePath, ocClient)).thenReturn(fakeResult);

        final boolean loadSuccess = mockLoaderSpied.load(folder);
        Assert.assertFalse("Loading remote folder resulting in 204 but without any RemoteFile should return false", loadSuccess);
    }


    /**
     * Test the whole behaviour of localFileLister with a fake file's tree
     *
     *     ## File tree
     *     >Folder 1 (unChanged)
     *             --> File 1 (should be ignored)
     *             --> Folder 2 (unsynced dir)
     *             ----> File 2 (new file should be added)
     *             ----> .File 3 (should be ignored)
     *   >Folder 3 (unChanged)
     *             -->Folder4 (skip because folder 3 unchanged)
     *             ----> File 4 (skip because folder 3 unchanged)
     *             -->.Folder5 (skip: hidden dir)
     *            ----> File 5 (skip because directory is hidden)
     *
     *   There is also the missing remote folder, which is expected to be in the result's folder's id
     */
    @Test
    public void test_listContentToScan() {
        prepareFakeContent();
        Assert.assertTrue("File listing failed", fileListerUnderTest.listContentToScan(context));

        final List<Long> foldersId = fileListerUnderTest.getSyncedFoldersId();
        Assert.assertEquals("List of folders to scan's id should contains 2 but contained: " + foldersId.size(), 2, foldersId.size());

        final List<RemoteFile> contentListed = fileListerUnderTest.getContentToScan();
        Assert.assertEquals("ContentListed should have 1 files but contained: " + contentListed.size(), 1, contentListed.size());
    }

    /**
     * Generate fake file tree & mock object for a full test of RemoteFileLister
     */
    private void prepareFakeContent() {
        /*
        * Generate folders
        */
        final String folder1Path = TEST_FILE_TREE_ROOT_PATH + "folder1/";
        final RemoteFile folder1 = Mockito.mock(RemoteFile.class); //unchanged
        when(folder1.getRemotePath()).thenReturn(folder1Path);
        when(folder1.getEtag()).thenReturn("123456789");
        when(folder1.getMimeType()).thenReturn("DIR");

        final String folder2Path = folder1Path + "folder2/";
        final RemoteFile folder2 = Mockito.mock(RemoteFile.class); //updated one
        when(folder2.getRemotePath()).thenReturn(folder2Path);
        when(folder2.getEtag()).thenReturn("234567891");
        when(folder2.getMimeType()).thenReturn("DIR");

        final String folder3Path = TEST_FILE_TREE_ROOT_PATH + "folder3/";
        final RemoteFile folder3 = Mockito.mock(RemoteFile.class); //unchanged
        when(folder3.getRemotePath()).thenReturn(folder3Path);
        when(folder3.getEtag()).thenReturn("345678912");
        when(folder3.getMimeType()).thenReturn("DIR");

        final String folder4Path = folder3Path + "folder4/";
        final RemoteFile folder4 = Mockito.mock(RemoteFile.class); //new one
        when(folder4.getRemotePath()).thenReturn(folder4Path);
        when(folder4.getEtag()).thenReturn("456789123");
        when(folder4.getMimeType()).thenReturn("DIR");

        final String folder5Path = folder3.getRemotePath() + ".folder5/";
        final RemoteFile folder5 = Mockito.mock(RemoteFile.class); //hidden one
        when(folder5.getRemotePath()).thenReturn(folder5Path);
        when(folder5.getEtag()).thenReturn("567891234");
        when(folder5.getMimeType()).thenReturn("DIR");

        /*
        * Generate files
        */
        final RemoteFile file1 = Mockito.mock(RemoteFile.class); //ignored because dir unchanged
        when(file1.getRemotePath()).thenReturn(folder1Path + "file1.png");
        when(file1.getMimeType()).thenReturn("any");
        final RemoteFile file2 = Mockito.mock(RemoteFile.class); //new file one, should be added
        when(file2.getRemotePath()).thenReturn(folder2Path + "file2.png");
        when(file2.getMimeType()).thenReturn("any");
        final RemoteFile file3 = Mockito.mock(RemoteFile.class); //hidden one: ignored
        when(file3.getRemotePath()).thenReturn(folder2Path + ".file3.png");
        when(file3.getMimeType()).thenReturn("any");
        final RemoteFile file4 = Mockito.mock(RemoteFile.class); //new one should be added
        when(file4.getRemotePath()).thenReturn(folder4Path + "file4.png");
        when(file4.getMimeType()).thenReturn("any");
        final RemoteFile file5 = Mockito.mock(RemoteFile.class); //ignored because dir is hidden
        when(file5.getRemotePath()).thenReturn(folder5Path + "file5.png");
        when(file5.getMimeType()).thenReturn("any");

        /*
        * Generate SyncedFolders
        */
        final SyncedFolder syncedFolder1 = new SyncedFolder("any", "local/folder1/", folder1Path, true);
        syncedFolder1.setLastEtag(folder1.getEtag());
        syncedFolder1.setId((int) DbHelper.insertSyncedFolder(syncedFolder1, context));

        final SyncedFolder syncedFolder2 = new SyncedFolder("any", "local/folder1/folder2/", folder2Path, true);
        syncedFolder2.setLastEtag("");
        syncedFolder2.setId((int) DbHelper.insertSyncedFolder(syncedFolder2, context));

        final SyncedFolder syncedFolder3 = new SyncedFolder("any","local/folder3/", folder3Path, true);
        syncedFolder3.setLastEtag(folder3.getEtag());
        syncedFolder3.setId((int) DbHelper.insertSyncedFolder(syncedFolder3, context));

        final SyncedFolder syncedFolderRemoved = new SyncedFolder("any", "local/missingFolder/", TEST_FILE_TREE_ROOT_PATH + "lambda/", true);
        syncedFolderRemoved.setLastModified(987654321L);
        syncedFolderRemoved.setLastEtag("123456789");
        syncedFolderRemoved.setId((int) DbHelper.insertSyncedFolder(syncedFolderRemoved, context));

        final ArrayList<SyncedFolder> syncedFolders = new ArrayList<>();
        syncedFolders.add(syncedFolder1);
        syncedFolders.add(syncedFolder2);
        syncedFolders.add(syncedFolder3);
        syncedFolders.add(syncedFolderRemoved);

        fileListerUnderTest = Mockito.spy(new RemoteFileLister(syncedFolders, ocClient));


        /* Mock the method depending to network and cloud */
        final RemoteFileLister.RemoteFolderLoader spiedDirLoader = Mockito.spy(fileListerUnderTest.createFolderLoader());

        final RemoteOperationResult folder1Result = Mockito.mock(RemoteOperationResult.class);
        when(folder1Result.getHttpCode()).thenReturn(207);
        when(folder1Result.isSuccess()).thenReturn(true);
        final ArrayList<RemoteFile> folder1Data = new ArrayList<>();
        folder1Data.add(folder1);
        folder1Data.add(file1);
        folder1Data.add(folder2);
        when(folder1Result.getData()).thenReturn(folder1Data);
        when(spiedDirLoader.readRemoteFolder(syncedFolder1.getRemoteFolder(), ocClient)).thenReturn(folder1Result);

        final RemoteOperationResult folder2Result = Mockito.mock(RemoteOperationResult.class);
        when(folder2Result.getHttpCode()).thenReturn(207);
        when(folder2Result.isSuccess()).thenReturn(true);
        final ArrayList<RemoteFile> folder2Data = new ArrayList<>();
        folder2Data.add(folder2);
        folder2Data.add(file2);
        folder2Data.add(file3);
        when(folder2Result.getData()).thenReturn(folder2Data);
        when(spiedDirLoader.readRemoteFolder(syncedFolder2.getRemoteFolder(), ocClient)).thenReturn(folder2Result);

        final RemoteOperationResult folder3Result = Mockito.mock(RemoteOperationResult.class);
        when(folder3Result.getHttpCode()).thenReturn(207);
        when(folder3Result.isSuccess()).thenReturn(true);
        final ArrayList<RemoteFile> folder3Data = new ArrayList<>();
        folder3Data.add(folder3);
        folder3Data.add(folder4);
        folder3Data.add(folder5);
        when(folder3Result.getData()).thenReturn(folder3Data);
        when(spiedDirLoader.readRemoteFolder(syncedFolder3.getRemoteFolder(), ocClient)).thenReturn(folder3Result);


        final RemoteOperationResult folder4Result = Mockito.mock(RemoteOperationResult.class);
        when(folder4Result.getHttpCode()).thenReturn(207);
        when(folder4Result.isSuccess()).thenReturn(true);
        final ArrayList<RemoteFile> folder4Data = new ArrayList<>();
        folder4Data.add(folder4);
        folder4Data.add(file4);
        when(folder4Result.getData()).thenReturn(folder4Data);
        when(spiedDirLoader.readRemoteFolder(folder4.getRemotePath(), ocClient)).thenReturn(folder4Result);

        final RemoteOperationResult folder5Result = Mockito.mock(RemoteOperationResult.class);
        when(folder5Result.getHttpCode()).thenReturn(207);
        when(folder5Result.isSuccess()).thenReturn(true);
        final ArrayList<RemoteFile> folder5Data = new ArrayList<>();
        folder5Data.add(folder4);
        folder5Data.add(file4);
        when(folder5Result.getData()).thenReturn(folder5Data);
        when(spiedDirLoader.readRemoteFolder(folder5.getRemotePath(), ocClient)).thenReturn(folder5Result);

        final RemoteOperationResult missingFolderResult = Mockito.mock(RemoteOperationResult.class);
        when(missingFolderResult.getHttpCode()).thenReturn(404);
        when(missingFolderResult.isSuccess()).thenReturn(false);
        when(spiedDirLoader.readRemoteFolder(syncedFolderRemoved.getRemoteFolder(), ocClient)).thenReturn(missingFolderResult);

        when(fileListerUnderTest.createFolderLoader()).thenReturn(spiedDirLoader);
    }
}
