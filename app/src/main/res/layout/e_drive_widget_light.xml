<!--
  ~ Copyright (C) 2024 MURENA SAS
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:background="@drawable/widget_background"
    android:paddingBottom="16dp"
    tools:background="@color/e_background_light">

    <TextView
        android:id="@+id/name"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_marginStart="16dp"
        android:layout_marginTop="16dp"
        android:textColor="@color/widget_text_color_light"
        android:textFontWeight="400"
        android:textIsSelectable="false"
        android:textSize="16sp"
        tools:targetApi="p"
        tools:text="John Doe" />

    <ImageView
        android:id="@+id/settings"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_alignParentEnd="true"
        android:contentDescription="@string/settings_title"
        android:paddingVertical="18dp"
        android:paddingStart="18dp"
        android:paddingEnd="16dp"
        android:src="@drawable/ic_settings_light" />

    <TextView
        android:id="@+id/email"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/name"
        android:layout_marginHorizontal="16dp"
        android:layout_marginTop="32dp"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="700"
        android:textIsSelectable="false"
        android:textSize="16sp"
        tools:ignore="RelativeOverlap,UnusedAttribute"
        tools:text="john.doe@murena.io" />

    <TextView
        android:id="@+id/show_alias"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/name"
        android:layout_alignParentEnd="true"
        android:layout_marginTop="16dp"
        android:paddingVertical="16dp"
        android:paddingStart="16dp"
        android:paddingEnd="12dp"
        android:text="@string/view_alias"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="400"
        android:textSize="16sp"
        app:drawableEndCompat="@drawable/ic_keyboard_arrow_down"
        app:drawableTint="@color/widget_text_color2_light"
        tools:ignore="RelativeOverlap"
        tools:targetApi="p" />

    <LinearLayout
        android:id="@+id/alias1_container"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/email"
        android:layout_alignParentStart="true"
        android:layout_marginTop="32dp"
        android:orientation="horizontal"
        android:visibility="gone"
        tools:visibility="visible">

        <TextView
            android:id="@+id/alias1"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginHorizontal="16dp"
            android:layout_weight="1"
            android:textColor="@color/widget_text_color_light"
            android:textFontWeight="400"
            android:textIsSelectable="false"
            android:textSize="16sp"
            tools:targetApi="p"
            tools:text="123quatro@murena.io" />

        <TextView
            android:id="@+id/alias1_clipboard"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:drawablePadding="10dp"
            android:padding="16dp"
            android:text="@string/copy"
            android:textColor="@color/widget_text_color2_light"
            android:textFontWeight="400"
            android:textSize="16sp"
            app:drawableEndCompat="@drawable/ic_clipboard"
            app:drawableTint="@color/widget_text_color2_light"
            tools:drawableEnd="@drawable/ic_clipboard"
            tools:targetApi="p" />
    </LinearLayout>

    <TextView
        android:id="@+id/hide_alias"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/alias1_container"
        android:layout_centerHorizontal="true"
        android:layout_marginTop="40dp"
        android:drawablePadding="10dp"
        android:padding="16dp"
        android:text="@string/hide_alias"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="400"
        android:textSize="16sp"
        android:visibility="gone"
        app:drawableEndCompat="@drawable/ic_arrow_up"
        app:drawableTint="@color/widget_text_color2_light"
        tools:targetApi="p"
        tools:visibility="visible" />

    <TextView
        android:id="@+id/status"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/hide_alias"
        android:layout_alignParentStart="true"
        android:layout_marginHorizontal="16dp"
        android:layout_marginTop="48dp"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="500"
        android:textIsSelectable="false"
        android:textSize="11sp"
        tools:targetApi="p"
        tools:text="10 GB OF 20 GB USED" />

    <ProgressBar
        android:id="@+id/progress"
        style="@android:style/Widget.Material.ProgressBar.Horizontal"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/status"
        android:layout_alignParentStart="true"
        android:layout_marginHorizontal="16dp"
        android:layout_marginTop="20dp"
        android:progressTint="@color/widget_progressBar"
        tools:progress="50" />

    <TextView
        android:id="@+id/sync"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/progress"
        android:layout_alignParentEnd="true"
        android:layout_marginHorizontal="16dp"
        android:layout_marginTop="16dp"
        android:text="@string/last_synced"
        android:textColor="@color/widget_text_color_light"
        android:textFontWeight="400"
        android:textSize="12sp"
        tools:targetApi="p" />

    <TextView
        android:id="@+id/plan"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/status"
        android:layout_alignParentStart="true"
        android:layout_marginStart="16dp"
        android:layout_marginTop="70dp"
        android:text="@string/my_plan"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="400"
        android:textSize="14sp"
        tools:targetApi="p" />

    <TextView
        android:id="@+id/planName"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/plan"
        android:layout_toEndOf="@id/plan"
        android:textColor="@color/widget_text_color_light"
        android:textFontWeight="400"
        android:textIsSelectable="false"
        android:textSize="14sp"
        tools:targetApi="p"
        tools:text="Premium 20 GB" />

    <Button
        android:id="@+id/upgrade"
        style="@android:style/Widget.Material.Button.Borderless"
        android:layout_width="wrap_content"
        android:layout_height="32dp"
        android:layout_alignTop="@id/status"
        android:layout_alignParentEnd="true"
        android:layout_marginTop="65dp"
        android:layout_marginEnd="16dp"
        android:background="@drawable/button_background_light"
        android:text="@string/upgrade"
        android:textColor="@color/widget_text_color2_light"
        android:textFontWeight="500"
        tools:targetApi="p" />
</RelativeLayout>
