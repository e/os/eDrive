/*
 * Copyright (C) 2025 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package foundation.e.drive.account

import android.accounts.AccountManager
import android.app.Application
import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import androidx.work.WorkManager
import foundation.e.drive.database.DbHelper
import foundation.e.drive.database.FailedSyncPrefsManager
import foundation.e.drive.synchronization.SyncProxy
import foundation.e.drive.utils.AppConstants
import foundation.e.drive.utils.AppConstants.INITIAL_FOLDER_NUMBER
import foundation.e.drive.utils.AppConstants.SETUP_COMPLETED
import foundation.e.drive.utils.DavClientProvider
import timber.log.Timber
import java.io.File

class AccountRemover(
    private val context: Context,
) {
    private val preferences: SharedPreferences by lazy {
        context.applicationContext.getSharedPreferences(
            AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE
        )
    }

    fun removeAccount() {
        cancelWorkers()
        setSyncStateToIdle()
        deleteDatabase()
        cleanSharedPreferences()
        removeCachedFiles()
        deleteNotificationChannels()
        cleanUpDavClient()
    }

    private fun cleanUpDavClient() {
        DavClientProvider.getInstance().cleanUp()
    }

    private fun setSyncStateToIdle() {
        SyncProxy.moveToIdle(context.applicationContext as Application)
    }

    private fun cancelWorkers() {
        val workManager = WorkManager.getInstance(context)
        workManager.cancelAllWorkByTag(AppConstants.WORK_GENERIC_TAG)
    }

    private fun deleteDatabase() {
        val result = context.applicationContext.deleteDatabase(DbHelper.DATABASE_NAME)
        Timber.d("Remove Database: %s", result)
    }

    private fun cleanSharedPreferences() {
        if (!context.applicationContext.deleteSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME)) {
            //If removal failed, clear all data inside
            preferences.edit().remove(AccountManager.KEY_ACCOUNT_NAME)
                .remove(AccountManager.KEY_ACCOUNT_TYPE)
                .remove(SETUP_COMPLETED)
                .remove(INITIAL_FOLDER_NUMBER)
                .remove(AppConstants.KEY_LAST_SCAN_TIME)
                .apply()
        }
        context.applicationContext.deleteSharedPreferences(FailedSyncPrefsManager.PREF_NAME)
    }

    private fun removeCachedFiles() {
        try {
            deleteDir(context.applicationContext.externalCacheDir)
        } catch (e: SecurityException) {
            Timber.e(e, "failed to delete cached file on account removal call")
        }
    }

    @Throws(SecurityException::class)
    private fun deleteDir(dir: File?): Boolean {
        if (dir == null) {
            Timber.w("cache file returned null. preventing a NPE")
            return false
        }

        return dir.deleteRecursively()
    }

    @Suppress("TooGenericExceptionCaught")
    private fun deleteNotificationChannels() {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        try {
            notificationManager.cancelAll()
        } catch (exception: RuntimeException) {
            Timber.e(exception, "Cannot cancel all notifications")
        }
    }
}
