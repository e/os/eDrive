/*
 * Copyright (C) 2025 e Foundation
 * Copyright (C) MURENA SAS 2023-2024
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.drive.account.receivers

import android.accounts.AccountManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import foundation.e.drive.account.AccountAdder
import timber.log.Timber

/**
 * Entry point for eDrive
 * Triggered by AccountManager
 * @author Vincent Bourgmayer
 */
class AccountAddedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d("\"Account added\" intent received")

        if (context == null || intent == null || intent.extras == null) return

        val extras = intent.extras!!
        val accountName = extras.getString(AccountManager.KEY_ACCOUNT_NAME, "")
        val accountType = extras.getString(AccountManager.KEY_ACCOUNT_TYPE, "")

        Timber.d("AccountAddedReceiver.onReceive with name: %s and type: %s", accountName, accountType)

        val accountAdder = AccountAdder(context)
        accountAdder.addAccount(accountName, accountType)
    }
}
