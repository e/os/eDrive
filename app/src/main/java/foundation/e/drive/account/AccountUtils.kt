/*
 * Copyright MURENA SAS 2022-2024
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.account

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import foundation.e.drive.R
import foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_GROUPS
import foundation.e.drive.utils.AppConstants.SHARED_PREFERENCE_NAME

object AccountUtils {

    @JvmStatic
    fun getPremiumPlan(accountManager: AccountManager, account: Account?): String? {
        if (account == null) return null
        val groupData = accountManager.getUserData(account, ACCOUNT_DATA_GROUPS)
        val premiumGroup = extractPremiumGroup(groupData)
        return extractPremiumPlan(premiumGroup)
    }

    @JvmStatic
    private fun extractPremiumPlan(premiumGroup: String?): String? {
        if (premiumGroup.isNullOrEmpty()) return null

        val splitPremiumGroup = premiumGroup.split("-")
        return if (splitPremiumGroup.size < 2) null
        else splitPremiumGroup[1]
    }

    @JvmStatic
    private fun extractPremiumGroup(groupData: String?): String? {
        if (groupData.isNullOrEmpty()) return null

        val groups = groupData.split(",")
        return groups.firstOrNull { group: String -> group.contains("premium-") }
    }

    @JvmStatic
    fun getAccount(context: Context): Account? {
        val prefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        val accountName = prefs.getString(AccountManager.KEY_ACCOUNT_NAME, "")

        return getAccount(accountName!!, context)
    }

    @JvmStatic
    fun getAccount(accountName: String, context: Context): Account? {
        if (accountName.isEmpty()) return null

        val accountManager = AccountManager.get(context)
        val accountType = context.getString(R.string.eelo_account_type)

        return accountManager.getAccountsByType(accountType)
            .firstOrNull { account -> account.name == accountName }
    }

    @JvmStatic
    fun isAccountAvailable(context: Context): Boolean {
        val accountManager = AccountManager.get(context.applicationContext)
        val accountList =
            accountManager.getAccountsByType(context.getString(R.string.eelo_account_type))
        return accountList.isNotEmpty()
    }
}
