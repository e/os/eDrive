/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.account;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nextcloud.common.NextcloudClient;
import com.nextcloud.operations.GetMethod;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

/**
 * @author TheScarastic
 * @author vincent Bourgmayer
 * /!\ Doesn't require NextcloudClient yet
 */
public class GetAliasOperation extends RemoteOperation<ArrayList<String>> {

    private static final String ALIAS_PATH = "/ocs/v1.php/cloud/hide-my-email/";

    // JSON Node names
    private static final String NODE_OCS = "ocs";
    private static final String NODE_DATA = "data";
    private static final String NODE_ALIASES = "aliases";
    private final String mID;

    private boolean isSuccess(int status) {
        return (status == HttpStatus.SC_OK);
    }

    public GetAliasOperation(@NonNull String id) {
        mID = id;
    }

    @Override
    @NonNull
    public RemoteOperationResult<ArrayList<String>> run(@NonNull NextcloudClient client) {
        GetMethod getMethod = null;
        RemoteOperationResult<ArrayList<String>> result;

        final String uri = client.getBaseUri() + ALIAS_PATH + mID;

        try {
            getMethod = new GetMethod(uri, true);
            final Map<String, String> queryString = new HashMap<>();
            queryString.put("format", "json");

            getMethod.setQueryString(queryString);

            final int status = getMethod.execute(client);

            if (isSuccess(status)) {
                final String response = getMethod.getResponseBodyAsString();
                final JSONArray aliases = parseResponse(response);
                final ArrayList<String> resultAliases = new ArrayList<>();

                result = new RemoteOperationResult<>(true, getMethod);

                if (aliases != null) {
                    for (int i = 0; i < aliases.length(); i++) {
                        resultAliases.add(aliases.get(i).toString());
                    }
                }
                result.setResultData(resultAliases);
            } else {
                result = new RemoteOperationResult<>(false, getMethod);
            }

        } catch (Exception exception){
            result = new RemoteOperationResult<>(exception);
            Timber.d(exception, "Fetching aliases failed");
        } finally {
            if (getMethod != null) {
                getMethod.releaseConnection();
            }
        }
        return result;
    }

    @Nullable
    public JSONArray parseResponse(@NonNull String response) {
        JSONArray result = null;
        try {
            final JSONObject jsonResponse = new JSONObject(response).optJSONObject(NODE_OCS);
            if (jsonResponse == null) return result;

            final JSONObject jsonData = jsonResponse.optJSONObject(NODE_DATA);
            if (jsonData == null) return result;

            result = jsonData.optJSONArray(NODE_ALIASES);
        } catch (JSONException e) {
            Timber.e(e);
        }
        return result;
    }
}
