/*
 * Copyright (C) 2025 e Foundation
 * Copyright (C) ECORP SAS 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.account.receivers;

import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import foundation.e.drive.R;
import foundation.e.drive.account.AccountRemover;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.ViewUtils;
import timber.log.Timber;

public class AccountRemoveCallbackReceiver extends BroadcastReceiver {

    private static final String ACTION_ACCOUNT_REMOVED = "android.accounts.action.ACCOUNT_REMOVED";

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        Timber.d("Received account remove broadcast request");

        final Context applicationContext = context.getApplicationContext();

        final SharedPreferences preferences = applicationContext.getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        if (!shouldProceedWithRemoval(intent, preferences, applicationContext)) {
            return;
        }

        AccountRemover accountRemover = new AccountRemover(context);
        accountRemover.removeAccount();

        ViewUtils.updateWidgetView(applicationContext);
    }

    private boolean shouldProceedWithRemoval(@NonNull Intent intent, @NonNull SharedPreferences preferences, @NonNull Context context) {
        if (isInvalidAction(intent) || intent.getExtras() == null) {
            Timber.w("Invalid account removal request");
            return false;
        }

        String currentAccount = preferences.getString(AccountManager.KEY_ACCOUNT_NAME, "");
        String currentAccountType = context.getString(R.string.eelo_account_type);

        if (currentAccount.isEmpty()) {
            Timber.d("No account set up, ignoring account removal");
            return false;
        }

        final String accountType = intent.getExtras().getString(AccountManager.KEY_ACCOUNT_TYPE);
        final String accountName = intent.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);

        return (currentAccount.equals(accountName) && currentAccountType.equals(accountType));
    }

    private boolean isInvalidAction(@NonNull Intent intent) {
        return !ACTION_ACCOUNT_REMOVED.equals(intent.getAction());
    }
}
