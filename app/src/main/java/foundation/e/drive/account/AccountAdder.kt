/*
 * Copyright (C) 2025 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package foundation.e.drive.account

import android.accounts.AccountManager
import android.content.Context
import android.content.SharedPreferences
import foundation.e.drive.R
import foundation.e.drive.utils.AppConstants
import foundation.e.drive.utils.DavClientProvider
import foundation.e.drive.work.WorkLauncher
import timber.log.Timber

class AccountAdder(private val context: Context) {

    private val preferences: SharedPreferences by lazy {
        context.applicationContext.getSharedPreferences(
            AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE
        )
    }

    fun addAccount(name: String, type: String) {
        if (!canStart(name, type, preferences, context)) return

        updateAccountNameOnPreference(name)

        val workLauncher = WorkLauncher.getInstance(context)
        if (workLauncher.enqueueSetupWorkers(context)) {
            DavClientProvider.getInstance().cleanUp()
            workLauncher.enqueuePeriodicUserInfoFetching()
        }
    }

    private fun updateAccountNameOnPreference(name: String) {
        preferences.edit()
            .putString(AccountManager.KEY_ACCOUNT_NAME, name)
            .apply()
    }

    /**
     * Check that conditions to start are met:
     * - Setup has not already been done
     * - AccountName is not empty
     * - AccountType is /e/ account
     * - the account is effectively available through accountManager
     */
    private fun canStart(
        accountName: String,
        accountType: String,
        prefs: SharedPreferences,
        context: Context
    ): Boolean {
        if (isSetupAlreadyDone(prefs)) {
            Timber.w("Set up is already done, skipping account addition.")
            return false
        }

        if (accountName.isEmpty()) {
            Timber.w("Account name is empty, skipping account addition.")
            return false
        }

        if (isInvalidAccountType(accountType, context)) {
            Timber.w("Account type: $accountType is invalid, skipping account addition.")
            return false
        }

        if (!isExistingAccount(accountName, context)) {
            Timber.w("No account exists for $accountName, skipping account addition.")
            return false
        }

        return true
    }

    private fun isSetupAlreadyDone(prefs: SharedPreferences): Boolean {
        return prefs.getBoolean(AppConstants.SETUP_COMPLETED, false)
    }

    private fun isInvalidAccountType(accountType: String, context: Context): Boolean {
        val validAccountType = context.getString(R.string.eelo_account_type)
        return accountType != validAccountType
    }

    private fun isExistingAccount(accountName: String, context: Context): Boolean {
        return AccountUtils.getAccount(accountName, context) != null
    }
}
