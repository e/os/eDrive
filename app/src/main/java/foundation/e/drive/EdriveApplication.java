/*
 * Copyright (C) 2025 e Foundation
 * Copyright (C) ECORP SAS 2022-2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive;

import static timber.log.Timber.DebugTree;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import foundation.e.drive.database.FailedSyncPrefsManager;
import foundation.e.drive.fileObservers.FileObserverManager;
import foundation.e.drive.recovery.RecoveryManager;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.utils.ReleaseTree;
import foundation.e.lib.telemetry.Telemetry;
import timber.log.Timber;

/**
 * Class representing the eDrive application.
 * It is instantiated before any other class.
 *
 * @author Jonathan klee
 * @author Vincent Bourgmayer
 */
public class EdriveApplication extends Application {
    private FileObserverManager fileObserverManager = null;

    @Override
    public void onCreate() {
        super.onCreate();
        setupLogging();

        fileObserverManager = new FileObserverManager(getApplicationContext());

        CommonUtils.createNotificationChannel(getApplicationContext());

        final SharedPreferences prefs = getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);

        if (!isAccountStoredInPreferences(prefs)) {
            final Account account = CommonUtils.getAccount(getString(R.string.eelo_account_type), AccountManager.get(this));
            if (account == null) {
                return;
            }

            prefs.edit().putString(AccountManager.KEY_ACCOUNT_NAME, account.name)
                    .putString(AccountManager.KEY_ACCOUNT_TYPE, account.type)
                    .apply();
        }

        FailedSyncPrefsManager.getInstance(getApplicationContext()).clearPreferences();

        setupEdriveRecovery();
    }

    private void setupEdriveRecovery() {
        RecoveryManager recoveryManager = new RecoveryManager(getApplicationContext());
        recoveryManager.initiateRecovery();
    }

    synchronized public void startRecursiveFileObserver() {
        fileObserverManager.initializeObserving();
    }

    synchronized public void stopRecursiveFileObserver() {
        fileObserverManager.stopObserving();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Timber.i("System is low on memory. Application might get killed by the system.");
    }

    private void setupLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree());
            return;
        }

        Telemetry.init(BuildConfig.SENTRY_DSN, this, true);
        Timber.plant(new ReleaseTree());
    }

    private boolean isAccountStoredInPreferences(@NonNull SharedPreferences prefs) {
        return prefs.getString(AccountManager.KEY_ACCOUNT_NAME, null) != null;
    }
}
