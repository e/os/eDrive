/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.model.RemoteFile;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.utils.FileUtils;

/**
 * Implementation of AbstractFileLister with method adapted to remote content
 * @author vincent Bourgmayer
 */
public class RemoteFileLister extends AbstractFileLister<RemoteFile> {
    // protected to avoid SyntheticAccessor
    @NonNull
    protected OwnCloudClient client;

    public RemoteFileLister(@NonNull List<SyncedFolder> directories, @NonNull OwnCloudClient client) {
        super(directories);
        this.client = client;
    }

    @Override
    protected boolean skipSyncedFolder(@NonNull SyncedFolder folder) {
        return (folder.isMediaType()
                && FileUtils.getFileNameFromPath(folder.getRemoteFolder()).startsWith("."))
                || !folder.isScanRemote();
    }

    @Override
    protected boolean skipFile(@NonNull RemoteFile file) {
        final String fileName = FileUtils.getFileNameFromPath(file.getRemotePath());
        return fileName == null || fileName.isEmpty() || fileName.startsWith(".");
    }

    @Override
    protected boolean skipDirectory(@NonNull RemoteFile directory, @NonNull SyncedFolder syncedFolder, @NonNull Context context) {
        return directory.getEtag().equals(syncedFolder.getLastEtag())
                && !DbHelper.syncedFolderHasContentToDownload(syncedFolder.getId(), context);
    }

    @Override
    protected boolean isDirectory(@NonNull RemoteFile file) {
        return file.getMimeType().equals("DIR");
    }

    @Override
    @Nullable
    protected String getFileName(@NonNull RemoteFile file) {
        return FileUtils.getFileNameFromPath(file.getRemotePath());
    }

    @Override
    @NonNull
    protected RemoteFolderLoader createFolderLoader() {
        return new RemoteFolderLoader();
    }

    @Override
    protected void updateSyncedFolder(@NonNull SyncedFolder syncedFolder, @NonNull RemoteFile folder) {
        syncedFolder.setLastEtag(folder.getEtag());
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public class RemoteFolderLoader implements FolderLoader<RemoteFile> {
        private static final int HTTP_404 = 404;
        private FolderWrapper<RemoteFile> directory;

        @Override
        public boolean load(@NonNull SyncedFolder syncedFolder) {
            if (syncedFolder.getRemoteFolder() == null) return false;
            final RemoteOperationResult<RemoteFile> remoteOperationResult = readRemoteFolder(syncedFolder.getRemoteFolder(), client);

            if (!remoteOperationResult.isSuccess()) {
                if (remoteOperationResult.getHttpCode() == HTTP_404) {
                    directory = new FolderWrapper<>();
                    return true;
                }
                return false;
            }

            final List<Object> datas = remoteOperationResult.getData();
            if (datas == null || datas.size() < 1) return false;

            final int dataSize = datas.size();
            final RemoteFile remoteDir = (RemoteFile) datas.get(0);
            if (remoteDir == null) return false;

            directory = new FolderWrapper<>(remoteDir);

            final List<RemoteFile> subFiles = new ArrayList<>();
            for (Object o: datas.subList(1, dataSize)) {
                final RemoteFile subFile = (RemoteFile) o;
                if (subFile != null) {
                    subFiles.add(subFile);
                }
            }
            directory.addContent(subFiles);
            return true;
        }

        /**
         * Execute the Propfind query to list files under a directory
         * @param remotePath RemotePath of the directory to observe
         * @param client Client used to execute query
         * @return RemoteOperationResult
         */
        @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        @NonNull
        public RemoteOperationResult readRemoteFolder(@NonNull String remotePath, @NonNull OwnCloudClient client) {
            final ReadFolderRemoteOperation operation = new ReadFolderRemoteOperation(remotePath);
            return operation.execute(client);
        }

        @Override
        @NonNull
        public FolderWrapper<RemoteFile> getFolderWrapper() {
            return directory;
        }
    }
}