/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.owncloud.android.lib.resources.files.FileUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFolder;
import timber.log.Timber;

/**
 * Factorisation of codes used to list file on cloud and on device
 * Implementation will vary depending of it is RemoteFile or File instance
 * that we want to list
 * @author vincent Bourgmayer
 */
public abstract class AbstractFileLister <T> {

    /**
     * Check if the given syncedFolder can be skip
     * @param syncedFolder The syncedFolder instance
     * @return true if we can skip
     */
    protected abstract boolean skipSyncedFolder(@NonNull SyncedFolder syncedFolder);

    /**
     * Check if the file can be skip or if it should be added to the list to scan
     * @param file file Object to check
     * @return true if file must be ignored
     */
    protected abstract boolean skipFile(@NonNull T file);

    /**
     * If the given folder has not changed, compared to database's data, skip it
     * @param currentDirectory The real latest state of the directory
     * @param syncedFolder The last known state of the directory
     * @param context Context used to access database, to look for unsynced file for the given directory
     * @return true if we can skip this directory
     */
    protected abstract boolean skipDirectory(@NonNull T currentDirectory, @NonNull SyncedFolder syncedFolder, @NonNull Context context);
    protected abstract boolean isDirectory(@NonNull T file);

    @Nullable
    protected abstract String getFileName(@NonNull T file);

    @NonNull
    protected abstract FolderLoader<T> createFolderLoader();
    protected abstract void updateSyncedFolder(@NonNull SyncedFolder syncedFolder,@NonNull T folder);

    /**
     * This allows to use RemoteOperationResult for RemoteFileLister
     * todo: some optimization could be done with FolderWrapper<T>
     * @param <F> RemoteFile or File
     */
    /* package */ interface FolderLoader<F> {

        @NonNull
        FolderWrapper<F> getFolderWrapper();
        boolean load(@NonNull SyncedFolder folder);
    }

    protected final List<SyncedFolder> folders;
    private final List<T> contentToScan;
    protected boolean diveIntoSkippedDir = false; //to be overriden only

    /**
     *  constructor to be call with super
     * @param folders List of SyncedFolder to read
     */
    protected AbstractFileLister(@NonNull List<SyncedFolder> folders) {
        this.folders = folders;
        this.contentToScan = new ArrayList<>();
    }

    /**
     * Perform the listing of files
     * @param context
     * @return
     */
    public boolean listContentToScan(@NonNull Context context) {
        final ListIterator<SyncedFolder> iterator = folders.listIterator() ;
        boolean isSomeContentToSync = false;
        while (iterator.hasNext()) {
            final SyncedFolder syncedFolder = iterator.next();
            if (syncedFolder == null) continue;
            Timber.v("SyncedFolder : %s, %s, %s, %s, %s", syncedFolder.getLibelle(), syncedFolder.getLocalFolder(), syncedFolder.getLastModified(), syncedFolder.isScanLocal(), syncedFolder.getId());
            isSomeContentToSync =  hasDirectoryContentToScan(syncedFolder, iterator, context) || isSomeContentToSync;
        }

        if (isSomeContentToSync) {
            DbHelper.updateSyncedFolders(folders, context); //@todo: maybe do this when all contents will be synced.
        }
        return isSomeContentToSync;
    }

    /**
     * Detailed process for reading a single folder
     * @param folder the SyncedFolder to check
     * @param iterator iterator over list of SyncedFolder
     * @param context context used to call DB
     * @return true the directory has content to synchronized false otherwise
     */
    private boolean hasDirectoryContentToScan(@NonNull SyncedFolder folder, @NonNull ListIterator<SyncedFolder> iterator, @NonNull Context context) {

        final FolderLoader<T> dirLoader = createFolderLoader();
        if (skipSyncedFolder(folder)
                || !isSyncedFolderInDb(folder, context)
                || !dirLoader.load(folder)) {  // I try to get the real directory  (File or RemoteFile).
            iterator.remove();
            return false;
        }

        final FolderWrapper<T> currentDirectory = dirLoader.getFolderWrapper();

        if (currentDirectory.isMissing()) {
            return true;
        }

        if (skipDirectory(currentDirectory.getFolder(), folder, context)) {
            iterator.remove();
            folder.setToSync(false);
            /**
             * LocalFileLister need to be able to access subFiles even for skipped directory
             * because A folder's lastModified value won't change if a subfolder's content is changed, removed or created
             * RemoteFileLister is based on eTag, which change even if a subfolder's content is changed
             */
            if (!diveIntoSkippedDir) return false;
        } else {
            folder.setToSync(true);
        }
        updateSyncedFolder(folder, currentDirectory.getFolder());

        //todo: look where to put in subclasses : syncedFolder.setLastEtag(directory.getEtag()).setToSync(true);

        final List<T> dirContent = currentDirectory.getContent();
        if (dirContent != null && !dirContent.isEmpty()) {
            contentToScan.addAll(sortContent(iterator, folder, dirContent));
        }
        return true;
    }


    /**
     * Tell if the given syncedFolder has been persisted in database.
     * If it's not the case initally, it tries to persist it.
     * @param folder SyncedFolder to check for persistance
     * @param context context to contact database
     * @return false if not persisted in db
     */
    private boolean isSyncedFolderInDb(@NonNull SyncedFolder folder, @NonNull Context context) {
        if (folder.getId() > 0) return true;

        final int syncedFolderId = (int) DbHelper.insertSyncedFolder(folder, context); //It will return -1 if there is an error, like an already existing folder with same value
        if (syncedFolderId <= 0) {
            Timber.v("insertion of syncedFolder for %s failed: %s ", folder.getRemoteFolder(), syncedFolderId);
            return false;
        }

        folder.setId(syncedFolderId);
        return true;
    }


    /**
     * Split content of a folder: subfolder on one side are added into the iterator loop
     * while subfiles are added to result list
     * @param iterator iterator intance over list of SyncedFolder
     * @param folder the SyncFolder which own the content
     * @param dirContent Content to sort
     * @return List of subfiles to scan or empty list if nothing
     */
    @NonNull
    private List<T> sortContent(@NonNull ListIterator<SyncedFolder> iterator, @NonNull SyncedFolder folder, List<T> dirContent) {
        final List<T> result = new ArrayList<>();
        if (dirContent == null) return result;

        for (T file : dirContent) {
            if (file == null) continue;

            final String fileName = getFileName(file);
            if (fileName == null) continue;

            if (isDirectory(file)) {
                final SyncedFolder subSyncedFolder = new SyncedFolder(folder, fileName + FileUtils.PATH_SEPARATOR, 0L, "");//Need to set lastModified to 0 to handle it on next iteration
                iterator.add(subSyncedFolder);
                iterator.previous();
            } else if (folder.isToSync() && !skipFile(file)) {
                Timber.v("added %s into list of file to scan", fileName);
                result.add(file);
            }
        }

        return result;
    }

    /**
     * List of file to scan
     * @return List<File> or List<RemoteFile> is expected based on implementations
     */
    @NonNull
    public List<T> getContentToScan() {
        return contentToScan;
    }

    /**
     * Share the list of syncedFolder's ID for syncedFolder which has content to scan
     * @return List of syncedFolder ids
     */
    @NonNull
    public List<Long> getSyncedFoldersId() {
        final List<Long> result = new ArrayList<>();
        for (SyncedFolder folder : folders)  {
            result.add((long) folder.getId());
        }
        return result;
    }
}
