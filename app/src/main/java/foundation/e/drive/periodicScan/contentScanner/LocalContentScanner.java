/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner;

import static foundation.e.drive.models.SyncedFileStateKt.DO_NOT_SCAN;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_ON_CLOUD;
import static foundation.e.drive.models.SyncedFileStateKt.SCAN_ON_DEVICE;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.List;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncRequest;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.models.SyncedFolder;
import foundation.e.drive.utils.FileUtils;
import timber.log.Timber;

/**
 * Class to encapsulate function about scanning local file and
 * create syncRequest when needed
 * @author vincent Bourgmayer
 */
public class LocalContentScanner extends AbstractContentScanner<File>{

    public LocalContentScanner(@NonNull Context context, @NonNull List<SyncedFolder> syncedFolders) {
        super(context, syncedFolders);
        Timber.tag(LocalContentScanner.class.getSimpleName());
    }

    @Override
    protected void onMissingFile(@NonNull SyncedFileState fileState) {
        if (!fileState.hasBeenSynchronizedOnce()) return;

        final File file = new File(fileState.getLocalPath());

        if (file.exists()) {
            Timber.d("Expected %s to be missing. but it still exists", file.getAbsolutePath());
            return;
        }
        //todo: should we check if already sync disabled, and then remove it from DB ?
        Timber.d("Add 'Disable syncing' request for file: %s", file.getAbsolutePath());
        syncRequests.put(fileState.getId(), new SyncRequest(fileState, SyncRequest.Type.DISABLE_SYNCING));
    }

    @Override
    protected void onNewFileFound(@NonNull File file) {
        final String filePath = file.getAbsolutePath();
        final SyncedFolder parentDir = getParentSyncedFolder(filePath);
        if (parentDir == null) return;

        int scanScope = DO_NOT_SCAN;
        if (parentDir.isEnabled()) {
            if (parentDir.isScanRemote()) scanScope += SCAN_ON_CLOUD;
            if (parentDir.isScanLocal()) scanScope += SCAN_ON_DEVICE;
        }

        final SyncedFileState newSyncedFileState = new SyncedFileState(-1, file.getName(), filePath, parentDir.getRemoteFolder() + file.getName(), "", 0, parentDir.getId(), parentDir.isMediaType(), scanScope);

        final int storedId = DbHelper.manageSyncedFileStateDB(newSyncedFileState, "INSERT", context);
        if (storedId > 0) {
            newSyncedFileState.setId( storedId );
            Timber.d("Add upload SyncRequest for new file %s", filePath);
            syncRequests.put(storedId, new SyncRequest(newSyncedFileState, SyncRequest.Type.UPLOAD));
        }
    }

    @Override
    protected void onKnownFileFound(@NonNull File file, @NonNull SyncedFileState fileState) {
        if (fileState.getScanScope() == DO_NOT_SCAN) return;
        
        if (FileDiffUtils.getActionForFileDiff(file, fileState) == FileDiffUtils.Action.Upload) {
            Timber.d("Add upload SyncRequest for %s", file.getAbsolutePath());
            syncRequests.put(fileState.getId(), new SyncRequest(fileState, SyncRequest.Type.UPLOAD));
        }
    }

    @Override
    protected boolean isSyncedFolderParentOfFile(@NonNull SyncedFolder syncedFolder, @NonNull String dirPath) {
        return syncedFolder.getLocalFolder().equals(dirPath);
    }

    @Override
    protected boolean isFileMatchingSyncedFileState(@NonNull File file, @NonNull SyncedFileState fileState) {
        final String filePath = FileUtils.getLocalPath(file);
        final String localPath = fileState.getLocalPath();

        return localPath.equals(filePath);
    }
}