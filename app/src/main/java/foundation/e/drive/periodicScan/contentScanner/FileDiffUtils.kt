/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.periodicScan.contentScanner

import androidx.annotation.VisibleForTesting
import com.owncloud.android.lib.resources.files.model.RemoteFile
import foundation.e.drive.models.SyncedFileState
import foundation.e.drive.utils.AppConstants
import java.io.File

object FileDiffUtils {
    enum class Action { Upload, Download, Skip, UpdateDB }
    /**
     * Define what to do of RemoteFile for which we know the Database equivalent
     * @param remoteFile RemoteFile
     * @param fileState SyncedFileState instance
     * @return Action from Enum
     */
    @JvmStatic
    fun getActionForFileDiff(remoteFile: RemoteFile, fileState: SyncedFileState): Action {
        if (!hasEtagChanged(remoteFile, fileState)) {
            if (isCorruptedTimestamp(remoteFile.modifiedTimestamp)) return Action.Upload

            if (hasAlreadyBeenDownloaded(fileState)) return Action.Skip
        }
        val localFileSize = getLocalFileSize(fileState.localPath)
        if (isRemoteAndLocalSizeEqual(remoteFile.size, localFileSize)) {
            return Action.UpdateDB
        }
        return Action.Download
    }

    /**
     * Define what to do of local file for which we know the Database equivalent
     * @param localFile File instance representing a file on the device
     * @param fileState SyncedFileState instance. Containing data from Database
     * @return Action from Enum
     */
    @JvmStatic
    fun getActionForFileDiff(localFile: File, fileState: SyncedFileState): Action {
        val isFileMoreRecentThanDB = fileState.lastModified < localFile.lastModified()

        return if (isFileMoreRecentThanDB || !fileState.isLastEtagStored()) {
            Action.Upload
        } else Action.Skip
    }

    /**
     * Compare RemoteFile's eTag with the one stored in Database
     * @param remoteFile RemoteFile
     * @param fileState last store file's state
     * @return true if ETag
     */
    @VisibleForTesting
    @JvmStatic
    fun hasEtagChanged(remoteFile: RemoteFile, fileState: SyncedFileState): Boolean {
        return fileState.isLastEtagStored() && remoteFile.etag != fileState.lastEtag
    }

    /**
     * Indicate if the file has already been downloaded
     * or detected on the device
     * @param fileState SyncedFileState containing data from Database
     * @return true if localLastModified store in Database == 0
     */
    @VisibleForTesting
    @JvmStatic
    fun hasAlreadyBeenDownloaded(fileState: SyncedFileState): Boolean {
        return fileState.lastModified > 0
    }

    /**
     * Compare file size for remote file & local file
     * @param remoteFileSize RemoteFile.size. Note: it's equal to getLength() except for
     * folder where it also sum size of its content
     * @param localFileSize File.length()
     * @return true if remote file size is same as local file size
     */
    @VisibleForTesting
    @JvmStatic
    fun isRemoteAndLocalSizeEqual(remoteFileSize: Long, localFileSize: Long): Boolean {
        return remoteFileSize == localFileSize
    }


    /**
     * Check if timestamp is equal to max of unsigned int 32
     *
     * For yet unknown reason, some remote files have this value on cloud (DB & file system)
     * the only way to fix them is to force re upload of the file with correct value
     * @param timestampInMillisecond remote file timestamp (Long)
     * @return true if the timestamp is equal to max of unsigned int 32
     */
    @VisibleForTesting
    @JvmStatic
    fun isCorruptedTimestamp(timestampInMillisecond: Long): Boolean {
        return timestampInMillisecond >= AppConstants.CORRUPTED_TIMESTAMP_IN_MILLISECOND
    }

    /**
     * Fetch file size on device
     * @param path Path of the file
     * @return 0 if path is empty or file size
     */
    @VisibleForTesting
    @JvmStatic
    fun getLocalFileSize(path: String): Long {
        if (path.isEmpty()) return 0
        return File(path).length()
    }
}