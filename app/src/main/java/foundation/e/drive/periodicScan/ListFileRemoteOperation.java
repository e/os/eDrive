/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan;

import android.content.Context;

import androidx.annotation.NonNull;

import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.model.RemoteFile;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.periodicScan.contentScanner.RemoteFileLister;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFolder;
import timber.log.Timber;

/**
 * /!\ Doesn't require NextcloudClient yet
 * @author Vincent Bourgmayer
 */
public class ListFileRemoteOperation extends RemoteOperation<ArrayList<RemoteFile>> {

    private final List<SyncedFolder> syncedFolders;
    private final Context context;

    private final ArrayList<Long> updatedSyncedFoldersId;

    public ListFileRemoteOperation(@NonNull List<SyncedFolder> syncedFolders, @NonNull Context context) {
        Timber.tag(ListFileRemoteOperation.class.getSimpleName());
        this.syncedFolders = syncedFolders;
        this.context = context;
        updatedSyncedFoldersId = new ArrayList<>();
    }

    /**
     * Execute l'operation
     * @param ownCloudClient DAV client
     * @return List containing remoteFolder followed by remote files
     */
    @Override
    @NonNull
    protected RemoteOperationResult<ArrayList<RemoteFile>> run(@NonNull OwnCloudClient ownCloudClient) {
        final RemoteFileLister fileLister = new RemoteFileLister(syncedFolders, ownCloudClient);
        final boolean isContentToScan = fileLister.listContentToScan(context);

        final RemoteOperationResult result = new RemoteOperationResult<>(RemoteOperationResult.ResultCode.OK);
        if (isContentToScan) {
            DbHelper.updateSyncedFolders(this.syncedFolders, this.context);
        }
        result.setResultData(fileLister.getContentToScan());
        updatedSyncedFoldersId.addAll(fileLister.getSyncedFoldersId());

        return result;
    }

    /**
     * @return list of syncedFolder
     */
    @NonNull
    public List<Long> getSyncedFoldersId(){
        return this.updatedSyncedFoldersId;
    }
}
