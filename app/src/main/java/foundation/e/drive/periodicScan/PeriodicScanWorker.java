/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.periodicScan;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.work.OneTimeWorkType;
import foundation.e.drive.work.WorkRequestFactory;
import timber.log.Timber;

/**
 * Worker that trigger the chain of worker that really need to be periodic
 * This worker is required because we cannot chain periodic work through WorkerAPI
 * at the moment
 * @author vincent Bourgmayer
 */
public class PeriodicScanWorker extends Worker {
    public final static String UNIQUE_WORK_NAME = "periodicScanWork";
    public PeriodicScanWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            final WorkManager workManager = WorkManager.getInstance(getApplicationContext());
            final WorkRequestFactory workRequestFactory = WorkRequestFactory.INSTANCE;

            final List<OneTimeWorkRequest> workRequestsLists = new ArrayList<>();
            workRequestsLists.add(workRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.LIST_APPS, null));
            workRequestsLists.add(workRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.FULL_SCAN, null));

            workManager.beginUniqueWork(FullScanWorker.UNIQUE_WORK_NAME, ExistingWorkPolicy.KEEP, workRequestsLists)
                    .enqueue();

            return Result.success();
        } catch (Exception exception) {
            Timber.e(exception);
            return Result.retry();
        }
    }
}