/*
 * Copyright © MURENA SAS 2022-2024.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.work

import androidx.work.BackoffPolicy.LINEAR
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import foundation.e.drive.account.AccountUserInfoWorker
import foundation.e.drive.account.setup.FinishSetupWorker
import foundation.e.drive.account.setup.RootFolderSetupWorker
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_ENABLE
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_ID
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_LAST_ETAG
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_LAST_MODIFIED
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_LIBELLE
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_LOCAL_PATH
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_MEDIATYPE
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_REMOTE_PATH
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_SCAN_LOCAL
import foundation.e.drive.account.setup.RootFolderSetupWorker.DATA_KEY_SCAN_REMOTE
import foundation.e.drive.models.SyncedFolder
import foundation.e.drive.periodicScan.FullScanWorker
import foundation.e.drive.periodicScan.ListAppsWorker
import foundation.e.drive.periodicScan.PeriodicScanWorker
import foundation.e.drive.synchronization.SyncWorker
import foundation.e.drive.utils.AppConstants.WORK_GENERIC_TAG
import foundation.e.drive.utils.AppConstants.WORK_SETUP_TAG
import java.util.concurrent.TimeUnit.MINUTES


enum class PeriodicWorkType {
    FETCH_USER_INFO,
    PERIODIC_SCAN}

enum class OneTimeWorkType {
    FULL_SCAN,
    USER_TRIGGERED_FULL_SCAN,
    LIST_APPS,
    FETCH_USER_INFO,
    ROOT_FOLDER_SETUP,
    FINISH_SETUP,
    ONE_TIME_SYNC
}

object WorkRequestFactory {

    private const val PERIODIC_WORK_REPEAT_INTERVAL_IN_MIN = 30L
    private const val PERIODIC_SCAN_FLEX_TIME_IN_MIN = 5L

    /**
     * Builds an instance of PeriodicWorkRequest depending of the work type specified
     * @param type WorkType. Should be PERIODIC_SCAN or PERIODIC_USER_INFO
     * @return Periodic WorkRequest
     */
    fun createPeriodicWorkRequest(type: PeriodicWorkType): PeriodicWorkRequest {
        return when (type) {
            PeriodicWorkType.FETCH_USER_INFO -> createPeriodicGetUserInfoWorkRequest()
            PeriodicWorkType.PERIODIC_SCAN -> createPeriodicScanWorkRequest()
        }
    }

    /**
     * Builds an instance of OneTimeWorkRequest depending of the work type specified.
     * @param type Should be ONE_TIME_USER_INFO, or FIRST_START, or CREATE_REMOTE_DIR
     * or it will throw InvalidParameterException
     * @param syncedFolder @Nullable this parameter is required for CREATE_REMOTE_DIR work type. Throw an NPE if null.
     * @return OneTimeWorkRequest's instance.
     */
    fun createOneTimeWorkRequest(type: OneTimeWorkType, syncedFolder: SyncedFolder? = null): OneTimeWorkRequest {
        return when (type) {
            OneTimeWorkType.FULL_SCAN -> createFullScanWorkRequest(false)
            OneTimeWorkType.USER_TRIGGERED_FULL_SCAN -> createFullScanWorkRequest(true)
            OneTimeWorkType.LIST_APPS -> createAppListGenerationWorkRequest()
            OneTimeWorkType.FETCH_USER_INFO -> createGetUserInfoWorkRequest()
            OneTimeWorkType.ROOT_FOLDER_SETUP -> {
                if (syncedFolder == null) {
                    throw NullPointerException("Cannot create RootFolderSetupWorker without syncFolder")
                }
                createRootFolderSetupWorkRequest(syncedFolder)
            }
            OneTimeWorkType.FINISH_SETUP -> createFinishSetupWorkRequest()
            OneTimeWorkType.ONE_TIME_SYNC -> createSyncWorkRequest()
        }
    }

    /**
     * Creates a OneTimeWorkRequest instance for
     * a Full scan with constraints on network (CONNECTED) and battery (shouldn't be low)
     * @return instance of OneTimeWorkRequest
     */
    private fun createFullScanWorkRequest(forced: Boolean): OneTimeWorkRequest {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        val data = Data.Builder()
            .putBoolean(FullScanWorker.ACTION_FORCED_SYNC_KEY, forced)
            .build()

        return OneTimeWorkRequestBuilder<FullScanWorker>()
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .setConstraints(constraints)
            .setInputData(data)
            .addTag(WORK_GENERIC_TAG)
            .build()
    }

    /**
     * Creates a workRequest to generate file which contains list of installed apps
     * @return the workRequest
     */
    private fun createAppListGenerationWorkRequest(): OneTimeWorkRequest {
        return OneTimeWorkRequestBuilder<ListAppsWorker>()
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .addTag(WORK_GENERIC_TAG)
            .build()

    }

    /**
     * Instantiates a OneTimeWorkRequest to retrieve user info
     * @return instance of OneTimeWorkRequest
     */
    private fun createGetUserInfoWorkRequest(): OneTimeWorkRequest {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        return OneTimeWorkRequestBuilder<AccountUserInfoWorker>()
            .setConstraints(constraints)
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .addTag(WORK_GENERIC_TAG)
            .addTag(WORK_SETUP_TAG)
            .build()
    }

    /**
     * Creates a OneTime workRequest to create a remote folder
     * With constraints on network (unmetered) and battery (not low)
     * @param syncedFolder SyncedFolder instance with data about folder to create
     * @return Instance OneTimeWorkRequest
     */
    private fun createRootFolderSetupWorkRequest(syncedFolder: SyncedFolder): OneTimeWorkRequest {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresBatteryNotLow(true)
            .build()

        return OneTimeWorkRequestBuilder<RootFolderSetupWorker>()
            .setInputData(createDataFromSyncedFolder(syncedFolder))
            .setConstraints(constraints)
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .addTag(WORK_GENERIC_TAG)
            .addTag(WORK_SETUP_TAG)
            .build()
    }

    /**
     * Parses SyncedFolder instance in Data, used as data for WorkRequest
     * @param folder SyncedFolder instance
     * @return Data instance
     */
    private fun createDataFromSyncedFolder(folder: SyncedFolder): Data {
        return Data.Builder()
            .putInt(DATA_KEY_ID, folder.getId())
            .putString(DATA_KEY_LIBELLE, folder.getLibelle())
            .putString(DATA_KEY_LOCAL_PATH, folder.getLocalFolder())
            .putString(DATA_KEY_REMOTE_PATH, folder.getRemoteFolder())
            .putString(DATA_KEY_LAST_ETAG, folder.getLastEtag())
            .putLong(DATA_KEY_LAST_MODIFIED, folder.getLastModified())
            .putBoolean(DATA_KEY_SCAN_LOCAL, folder.isScanLocal())
            .putBoolean(DATA_KEY_SCAN_REMOTE, folder.isScanRemote())
            .putBoolean(DATA_KEY_ENABLE, folder.isEnabled())
            .putBoolean(DATA_KEY_MEDIATYPE, folder.isMediaType())
            .build()

    }

    /**
     * Creates a OneTime WorkRequest which finish setup process
     * @return Instance of OneTimeWorkRequest
     */
    private fun createFinishSetupWorkRequest(): OneTimeWorkRequest {
        return OneTimeWorkRequestBuilder<FinishSetupWorker>()
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .addTag(WORK_GENERIC_TAG)
            .addTag(WORK_SETUP_TAG)
            .build()

    }

    private fun createSyncWorkRequest(): OneTimeWorkRequest {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        return OneTimeWorkRequestBuilder<SyncWorker>()
            .setBackoffCriteria(LINEAR, 2, MINUTES)
            .setConstraints(constraints)
            .addTag(WORK_GENERIC_TAG)
            .build()
    }

    /**
     * Creates a PeriodicWorkRequest instance for
     * a Full scan with constraints on network (CONNECTED) and battery (not low)
     * @return instance of PeriodicWorkRequest
     */
    private fun createPeriodicScanWorkRequest(): PeriodicWorkRequest {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresBatteryNotLow(true)
            .build()

        return PeriodicWorkRequestBuilder<PeriodicScanWorker>(
            PERIODIC_WORK_REPEAT_INTERVAL_IN_MIN, MINUTES,
            PERIODIC_SCAN_FLEX_TIME_IN_MIN,MINUTES)
            .setConstraints(constraints)
            .addTag(WORK_GENERIC_TAG)
            .build()
    }

    private fun createPeriodicGetUserInfoWorkRequest(): PeriodicWorkRequest {
        val constraint = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        return PeriodicWorkRequestBuilder<AccountUserInfoWorker>(
            PERIODIC_WORK_REPEAT_INTERVAL_IN_MIN, MINUTES)
            .setConstraints(constraint)
            .addTag(WORK_GENERIC_TAG)
            .build()
    }
}
