/*
 * Copyright © MURENA SAS 2023-2024.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.work

import android.content.Context
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import foundation.e.drive.account.AccountUserInfoWorker
import foundation.e.drive.models.SyncedFolder
import foundation.e.drive.periodicScan.FullScanWorker
import foundation.e.drive.periodicScan.PeriodicScanWorker
import foundation.e.drive.synchronization.SyncWorker
import foundation.e.drive.utils.RootSyncedFolderProvider

/**
 * @author Vincent Bourgmayer
 */
class WorkLauncher private constructor(context: Context) {
    private val workManager: WorkManager

    init {
        workManager = WorkManager.getInstance(context)
    }

    companion object {
        private var instance: WorkLauncher? = null

        @JvmStatic
        fun getInstance(context: Context): WorkLauncher {
            return instance ?: WorkLauncher(context.applicationContext)
        }
    }

    fun enqueuePeriodicUserInfoFetching() {
        val request = WorkRequestFactory.createPeriodicWorkRequest(PeriodicWorkType.FETCH_USER_INFO)
        workManager.enqueueUniquePeriodicWork(
            AccountUserInfoWorker.UNIQUE_WORK_NAME,
            ExistingPeriodicWorkPolicy.UPDATE,
            request
        )
    }

    fun enqueuePeriodicFullScan() {
        val request = WorkRequestFactory.createPeriodicWorkRequest(PeriodicWorkType.PERIODIC_SCAN)
        workManager.enqueueUniquePeriodicWork(PeriodicScanWorker.UNIQUE_WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            request)
    }

    fun enqueueOneTimeFullScan(isForced: Boolean) {
        if (isForced) {
            val request = WorkRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.USER_TRIGGERED_FULL_SCAN,
                null)
            workManager.enqueueUniqueWork(FullScanWorker.UNIQUE_WORK_NAME,
                ExistingWorkPolicy.REPLACE,
                request)
            return
        }

        val request = WorkRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.FULL_SCAN,
            null)
        workManager.enqueueUniqueWork(FullScanWorker.UNIQUE_WORK_NAME,
            ExistingWorkPolicy.KEEP,
            request)
    }

    fun enqueueOneTimeAppListGenerator() {
        val request = WorkRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.LIST_APPS,
            null)
        workManager.enqueue(request)
    }

    fun enqueueOneTimeSync() {
        val request = WorkRequestFactory.createOneTimeWorkRequest(OneTimeWorkType.ONE_TIME_SYNC, null)
        workManager.enqueueUniqueWork(SyncWorker.UNIQUE_WORK_NAME, ExistingWorkPolicy.KEEP, request)
    }


    fun enqueueSetupWorkers(context: Context): Boolean {
        val rootFolderSetupWorkers = generateRootFolderSetupWorkers(context)
        if (rootFolderSetupWorkers.isEmpty()) {
            return false
        }

        val getUserInfoRequest = WorkRequestFactory.createOneTimeWorkRequest(
            OneTimeWorkType.FETCH_USER_INFO,
            null
        )

        val finishSetupRequest = WorkRequestFactory.createOneTimeWorkRequest(
            OneTimeWorkType.FINISH_SETUP,
            null
        )

        var workContinuation = WorkManager.getInstance(context)
            .beginWith(getUserInfoRequest)

        rootFolderSetupWorkers.forEach {
            workContinuation = workContinuation.then(it)
        }

        workContinuation
            .then(finishSetupRequest)
            .enqueue()

        return true
    }

    private fun generateRootFolderSetupWorkers(context: Context): List<OneTimeWorkRequest> {
        val rootSyncedFolderList: List<SyncedFolder> =
            RootSyncedFolderProvider.getSyncedFolderRoots(context)

        return rootSyncedFolderList.map {
            WorkRequestFactory.createOneTimeWorkRequest(
                OneTimeWorkType.ROOT_FOLDER_SETUP,
                it
            )
        }
    }
}
