/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.utils;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import timber.log.Timber;

/**
 * @author vincent Bourgmayer
 */
public class ReleaseTree extends Timber.DebugTree{
    private static boolean debugEnable = false;

    public static void allowDebugLogOnProd(boolean allow) {
        debugEnable = allow;
    }

    @Override
    protected void log(int priority, @Nullable String tag, @NonNull String message, @Nullable Throwable throwable) {
        if (!debugEnable && priority < Log.INFO ) {
            return;
        }

        super.log(priority, tag, message, throwable);
    }
}
