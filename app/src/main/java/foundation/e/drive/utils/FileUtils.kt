/*
 * Copyright (C) 2023-2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package foundation.e.drive.utils

import com.owncloud.android.lib.resources.files.FileUtils
import com.owncloud.android.lib.resources.files.FileUtils.PATH_SEPARATOR
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.net.URLConnection
import java.nio.file.FileSystemException
import java.nio.file.Files
import java.nio.file.StandardCopyOption

object FileUtils {
    /**
     * Return name of a file from its access path
     * Also work with folder path. If last char is PATH separator, then the last occurence is removed.
     *
     * @param rawPath File name will be extracted from this path.  Do not provide directory path
     * @return String, the last part after separator of path or null if invalid path has been provided
     */
    @JvmStatic
    fun getFileNameFromPath(rawPath: String): String? {
        val path = rawPath.removeSuffix(PATH_SEPARATOR)
        val lastIndexOfPathSeparator = path.lastIndexOf(FileUtils.PATH_SEPARATOR)
        if (lastIndexOfPathSeparator == -1) return null
        return path.substring(lastIndexOfPathSeparator + 1)
    }

    /**
     * Return the canonical path if available, either return absolute path
     *
     * @param file file from which we want the path
     * @return canonical path if available
     */
    @JvmStatic
    fun getLocalPath(file: File): String {
        val result: String = try {
            file.canonicalPath
        } catch (exception: SecurityException) {
            Timber.v(exception)
            file.absolutePath //todo why not simply always use getAbsolutePath?
        } catch (exception: IOException) {
            Timber.v(exception)
            file.absolutePath
        }
        return result
    }

    /**
     * Guess mimetype of the file from file extension
     *
     * URLConnection method throw NPE if no extension or unknown extension (e.g: .tmo)
     *
     * @param file the file for whom we want Mime type
     * @return String containing mimeType of the file for known type default value for others
     */
    @JvmStatic
    fun getMimeType(file: File): String {
        try {
            return URLConnection.guessContentTypeFromName(file.name)
        } catch(e: java.lang.NullPointerException) {
            return "*/*"
        }
    }

    @JvmStatic
    fun isPartFile(file: File) = file.name.endsWith(".part")

    @JvmStatic
    fun isNotPartFile(file: File) = !isPartFile(file)

    @JvmStatic
    fun copyFile(source: File, target: File): Boolean {
        var success = false
        try {
            val copyResult = Files.copy(
                source.toPath(),
                target.toPath(),
                StandardCopyOption.REPLACE_EXISTING
            )
            val copiedFile = copyResult.toFile()
            success = verifyCopyOperation(copiedFile, source)

            if (success) {
                source.delete()
            } else {
                copiedFile.delete()
            }

        } catch (fileSystemException: FileSystemException) {
            Timber.w(fileSystemException)
        } catch (unsupportedOperationException: UnsupportedOperationException ) {
            Timber.e(unsupportedOperationException)
        } catch (ioException: IOException) {
            Timber.e(ioException)
        }
        return success
    }

    @JvmStatic
    private fun verifyCopyOperation(copiedFile: File, tmpFile: File): Boolean {
        return copiedFile.length() == tmpFile.length()
    }

    @JvmStatic
     fun ensureDirectoryExists(directory: File): Boolean {
        if (!directory.exists()) {
            directory.mkdirs()
        }
        return directory.exists()
    }
}
