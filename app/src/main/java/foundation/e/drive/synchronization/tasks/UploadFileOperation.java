/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.synchronization.tasks;

import static foundation.e.drive.utils.FileUtils.getMimeType;

import android.accounts.Account;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.nextcloud.common.NextcloudClient;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.Quota;
import com.owncloud.android.lib.common.UserInfo;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.ChunkedFileUploadRemoteOperation;
import com.owncloud.android.lib.resources.files.CreateFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.ExistenceCheckRemoteOperation;
import com.owncloud.android.lib.resources.files.FileUtils;
import com.owncloud.android.lib.resources.files.ReadFileRemoteOperation;
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation;
import com.owncloud.android.lib.resources.files.model.RemoteFile;
import com.owncloud.android.lib.resources.users.GetUserInfoRemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import foundation.e.drive.database.DbHelper;
import foundation.e.drive.models.SyncedFileState;
import foundation.e.drive.utils.DavClientProvider;
import timber.log.Timber;

/**
 * @author Vincent Bourgmayer
 * High level Operation which upload a local file to a remote cloud storage
 */
@SuppressWarnings("rawtypes")
public class UploadFileOperation extends RemoteOperation {
    public final static int FILE_SIZE_FLOOR_FOR_CHUNKED = 3072000; //3MB
    private static final Set<ResultCode> handledFailureCodes = getHandledFailureCodes();

    private final Context context;
    private final SyncedFileState syncedState;
    private final Account account; //TODO Remove as soon as nextcloud library move all Operation to NextcloudClient instead of OwncloudClient

    /**
     * Construct an upload operation with an already known syncedFileState
     * @param syncedFileState syncedFileState corresponding to file.
     */
    public UploadFileOperation (@NonNull final SyncedFileState syncedFileState, @Nullable final Account account, @NonNull final Context context) {
        Timber.tag(UploadFileOperation.class.getSimpleName());
        this.syncedState = syncedFileState;
        this.context = context;
        this.account = account;
    }

    /**
     * Execute the operation:
     *
     * creates UploadFileOperation and execute it synchronously.
     * When result is get and if it's a success
     *  Start scan remoteFile to get remote file Etag and save it in the SyncedFileState database.
     * @param client (OwnCloudClient)
     * @return RemoteOperationResult. Contains "Unknown error" as result code if upload fails or can contains
     * "conflict" result code if it can got new Etag. It contains resultCode "Ok" if everything is allright or "sync_conflict" if file
     * hasn't change since last update or "forbidden" if no remote path can be fetch.
     */
    @Override
    @NonNull
    protected RemoteOperationResult run(@NonNull OwnCloudClient client ) {
        final File file = new File(syncedState.getLocalPath());

        final ResultCode conditionCheckResult = checkCondition(file, client);
        if (conditionCheckResult != ResultCode.OK) {
            return new RemoteOperationResult(conditionCheckResult);
        }

        final RemoteOperationResult<String> uploadResult;
        if (file.length() >= FILE_SIZE_FLOOR_FOR_CHUNKED) {
            Timber.d("Upload %s as chunked file", file.getName());
            uploadResult = uploadChunkedFile(file, client);
        } else {
            final boolean checkEtag = ifMatchETagRequired(client);
            uploadResult = uploadFile(file, client, checkEtag);
        }

        final ResultCode resultCode;
        if (uploadResult.isSuccess()) {
            final String latestEtag = getLatestEtag(uploadResult, client);
            updateSyncedFileState(latestEtag, file.lastModified());
            resultCode = uploadResult.getCode();
        } else {
            resultCode = onUploadFailure(uploadResult.getCode(), file.getName());
        }

        return new RemoteOperationResult(resultCode);
    }


    /**
     * Handle upload's failure
     * @param uploadResult ResultCode for upload
     * @param fileName name of the file
     * @return
     */
    private ResultCode onUploadFailure(final ResultCode uploadResult, final String fileName) {
        if (!handledFailureCodes.contains(uploadResult)) {
            Timber.d("Upload for %s failed : %s", fileName, uploadResult);
            return ResultCode.UNKNOWN_ERROR;
        }
        return uploadResult;
    }

    @NonNull
    private static Set<ResultCode> getHandledFailureCodes() {
        final Set<ResultCode> handledResultCodes = new HashSet<>();
        handledResultCodes.add(ResultCode.CONFLICT);
        handledResultCodes.add(ResultCode.QUOTA_EXCEEDED);
        handledResultCodes.add(ResultCode.WRONG_CONNECTION);
        handledResultCodes.add(ResultCode.NO_NETWORK_CONNECTION);
        handledResultCodes.add(ResultCode.HOST_NOT_AVAILABLE);

        return handledResultCodes;
    }

    /**
     * Check condition required to upload the file:
     * - the local file exist
     * - the local file is not already up to date with the remote one
     * - there is enough free storage
     * - the remote directory exists
     * @param file file to upload
     * @param client client used to execute some request
     * @return ResultCode.OK if everything is alright
     */
    private ResultCode checkCondition(final File file, final OwnCloudClient client) {
        if (file == null || !file.exists()) {
            Timber.d("Can't get the file. It might have been deleted");
            return ResultCode.FORBIDDEN;
        }

        //If file already up-to-date & synced
        if (syncedState.isLastEtagStored()
                && syncedState.getLastModified() == file.lastModified()) {
            Timber.d("Synchronization conflict because: last modified from DB(%s) and from file (%s) are the same ", syncedState.getLastModified(), file.lastModified());
            return ResultCode.SYNC_CONFLICT;
        }

        final NextcloudClient ncClient = DavClientProvider.getInstance().getNcClientInstance(account, context);
        if (ncClient == null) {
            return ResultCode.ACCOUNT_EXCEPTION;
        }

        final RemoteOperationResult checkQuotaResult = checkAvailableSpace(ncClient, file.length());
        DavClientProvider.getInstance().saveAccounts(context);

        if (checkQuotaResult.getCode() != ResultCode.OK) {
            Timber.d("Impossible to check quota. Cancels upload of %s", syncedState.getLocalPath());
            return checkQuotaResult.getCode();
        }

        final String targetPath = syncedState.getRemotePath();
        if (!createRemoteFolder(targetPath, client)) {
            return ResultCode.UNKNOWN_ERROR;
        }

        return ResultCode.OK;
    }

    /**
     * Update syncedFileState (etag & last modified) in case of successful upload
     * @param latestEtag new eTag of the remoteFile
     * @param fileLastModified value of local file's last modified
     */
    private void updateSyncedFileState(@Nullable final String latestEtag, final long fileLastModified) {
        if (latestEtag != null) {
            syncedState.setLastEtag(latestEtag);
        }

        syncedState.setLastModified(fileLastModified);
        DbHelper.manageSyncedFileStateDB(syncedState, "UPDATE", context);
    }

    private @Nullable String getLatestEtag(@NonNull final RemoteOperationResult<String> uploadResult, @NonNull final OwnCloudClient client) {

        if (uploadResult.getResultData() != null) {
            return uploadResult.getResultData();
        }

        //The below code should only be called for chunked upload. But
        //for some unknown reason, the simple file upload doesn't give the eTag in the result
        // so, I moved the code here as a security
        try {
            return readLatestEtagFromCloud(client);
        } catch (ClassCastException exception) {
            Timber.w("Impossible to read eTag from cloud: %s", exception.getMessage());
            return null;
        }
    }

    private @Nullable String readLatestEtagFromCloud(@NonNull final OwnCloudClient client) throws ClassCastException{
        final RemoteOperationResult result = readRemoteFile(syncedState.getRemotePath(), client);
        if (!result.isSuccess()) return null;

        final ArrayList resultData = result.getData();

        if (resultData == null || resultData.isEmpty()) {
            return null;
        }

        return ((RemoteFile) resultData.get(0)).getEtag();
    }


    /**
     * Perform an operation to check available space on server before to upload
     * @param client OwnCloudClient
     * @return RemoteOperationResult
     */
    @VisibleForTesting()
    @NonNull
    public RemoteOperationResult checkAvailableSpace(@NonNull NextcloudClient client, long fileSize) {
        final RemoteOperationResult<UserInfo> ocsResult = readUserInfo(client);
        if (!ocsResult.isSuccess()) {
            return ocsResult;
        }

        final Quota quotas = ocsResult.getResultData().getQuota();

        if (quotas != null && quotas.getFree() < fileSize) {
            return new RemoteOperationResult(ResultCode.QUOTA_EXCEEDED);
        }

        return ocsResult;
    }

    /**
     * Read user info to get Quota's data
     * note: this has been extracted from checkAvailableSpace for
     * testing purpose
     * @param client client to run the method
     * @return RemoteOperationResult
     */
    @VisibleForTesting()
    @NonNull
    public RemoteOperationResult<UserInfo> readUserInfo(@NonNull NextcloudClient client) {
        final GetUserInfoRemoteOperation GetUserInfoRemoteOperation = new GetUserInfoRemoteOperation();
        return GetUserInfoRemoteOperation.execute(client);
    }

    /**
     * Read remote file after upload to retrieve eTag
     * @param remotePath file's remote path
     * @param client Owncloudclient instance. @TODO will be replaced by NextcloudClient in future.
     * @return RemoteOperationResult instance containing failure details or RemoteFile instance
     */
    @VisibleForTesting()
    @NonNull
    public RemoteOperationResult readRemoteFile(@NonNull final String remotePath, @NonNull final OwnCloudClient client) {
        final ReadFileRemoteOperation readRemoteFile = new ReadFileRemoteOperation(remotePath);
        //noinspection deprecation
        return readRemoteFile.execute(client);
    }

    /**
     * Upload a chunked file
     * Used for file bigger than 3MB
     * @param file File to upload
     * @param client OwncloudClient to perform the upload. @TODO will be replaced by NextcloudClient in future.
     * @return RemoteOperationResult instance containing success or failure status with details
     */
    @VisibleForTesting()
    @NonNull
    public RemoteOperationResult uploadChunkedFile(@NonNull final File file, @NonNull final OwnCloudClient client) {
        final long timeStamp = formatTimestampToMatchCloud(file.lastModified());
        final String mimeType = getMimeType(file);
        final ChunkedFileUploadRemoteOperation uploadOperation = new ChunkedFileUploadRemoteOperation(syncedState.getLocalPath(),
                syncedState.getRemotePath(),
                mimeType, syncedState.getLastEtag(),
                timeStamp, false);

        //noinspection deprecation
        return uploadOperation.execute(client);
    }

    /**
     * Check that the remote file exists
     * @param remotePath path of remote file
     * @param client OwncloudClient instance to perform the request
     * @return true if the remote file exist, false otherwise
     */
    @VisibleForTesting()
    public boolean remoteFileExist(@NonNull String remotePath, @NonNull OwnCloudClient client) {
        final ExistenceCheckRemoteOperation operation = new ExistenceCheckRemoteOperation(remotePath, false);
        //noinspection deprecation
        final RemoteOperationResult result = operation.execute(client);
        return result.isSuccess();
    }

    /**
     * define if upload request should embed a "if-match" header
     * with eTag.
     *
     * - Only check for etag if the file can be downloaded, aka media files
     * (pictures, music, etc.)
     * - Only check for etag, if a previous eTag was already known
     * which means that the file has already been on the cloud
     * - Only check for eTag if the file is still on the cloud or we will get http 412
     *
     * @param client OwnCloudClient instance used to check if remote file exists
     * @return true if the if-match header should be added
     */
    @VisibleForTesting()
    public boolean ifMatchETagRequired(@NonNull OwnCloudClient client) {
        return syncedState.isMediaType()
                && syncedState.isLastEtagStored()
                && remoteFileExist(syncedState.getRemotePath(), client);
    }

    /**
     * Upload a file
     * note: this has been extracted from run(...) for
     * testing purpose
     * @param file File instance representing the file to upload
     * @param client client to run the method. TODO will be replaced by NextcloudClient in future.
     * @param checkEtag indicate if upload request must have a if-match header with eTag value
     * @return RemoteOperationResult the instance must contains etag in resultData if successful.
     */
    @VisibleForTesting()
    @NonNull
    public RemoteOperationResult<String> uploadFile(@NonNull final File file, @NonNull final OwnCloudClient client, boolean checkEtag) {
        final long timeStamp = formatTimestampToMatchCloud(file.lastModified());
        final String eTag = checkEtag ? syncedState.getLastEtag() : null;

        final UploadFileRemoteOperation uploadOperation = new UploadFileRemoteOperation(syncedState.getLocalPath(),
                syncedState.getRemotePath(),
                getMimeType(file),
                eTag, //If not null, it can cause error HTTP 412; which means remote file has change
                timeStamp);
        //noinspection deprecation
        return  uploadOperation.execute(client);
    }

    /**
     * Create remote parent folder of the file if missing
     * @param targetPath Path of remote directory to create or check for existence
     * @param client Client to perform the request. TODO will be replaced by NextcloudClient in future.
     * @return true if the parent directory has been created, false either
     */
    @VisibleForTesting()
    public boolean createRemoteFolder(@NonNull String targetPath, @NonNull OwnCloudClient client) {
        final String remoteFolderPath = targetPath.substring(0, targetPath.lastIndexOf(FileUtils.PATH_SEPARATOR) + 1);
        final CreateFolderRemoteOperation createFolderOperation = new CreateFolderRemoteOperation(remoteFolderPath, true);
        //noinspection deprecation
        final RemoteOperationResult createFolderResult = createFolderOperation.execute(client);
        return createFolderResult.isSuccess() || createFolderResult.getCode() == ResultCode.FOLDER_ALREADY_EXISTS;
    }

    @NonNull
    public SyncedFileState getSyncedState() {
        return syncedState;
    }

    /**
     * Convert local file timestamp to the format expected by the cloud
     * On Android, file timestamp is a ms value, while nextcloud expect value in second
     * @return String timestamp in second
     */
    @VisibleForTesting()
    public long formatTimestampToMatchCloud(long timestamp) {
        return timestamp / 1000;
    }
}