/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.synchronization

import android.accounts.Account
import android.content.Context
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode.HOST_NOT_AVAILABLE
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode.NO_NETWORK_CONNECTION
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode.WRONG_CONNECTION
import foundation.e.drive.database.DbHelper
import foundation.e.drive.database.FailedSyncPrefsManager
import foundation.e.drive.models.SyncRequest
import foundation.e.drive.models.SyncRequest.Type.UPLOAD
import foundation.e.drive.models.SyncRequest.Type.DISABLE_SYNCING
import foundation.e.drive.models.SyncRequest.Type.DOWNLOAD
import foundation.e.drive.models.SyncWrapper
import foundation.e.drive.synchronization.tasks.UploadFileOperation
import foundation.e.drive.utils.CommonUtils
import timber.log.Timber
import java.io.File

class SyncTask(
    val request: SyncRequest,
    val client: OwnCloudClient,
    val account: Account,
    val context: Context,
) : Runnable {
    companion object {
        private val syncManager = SyncProxy as SyncManager
    }

    private val fileName = request.syncedFileState.name
    private val fileLocalPath = request.syncedFileState.localPath
    private val syncNotifier = SyncProgressNotifier(context)


    override fun run() {
        if (!canStart()) {
            return
        }

        Timber.d(" starts ${request.operationType.name} for: $fileLocalPath")

        val wrapper = SyncWrapper(request, account, context)
        syncManager.addStartedRequest(fileLocalPath, wrapper)

        val succeed = when (request.operationType) {
            UPLOAD -> runUpload(wrapper)
            DOWNLOAD -> runDownload(wrapper)
            DISABLE_SYNCING -> runSyncDisabling()
        }

        syncNotifier.notifyTaskFinished(SyncWorker.pendingTaskCounter.decrementAndGet())
        updateFailureCounter(request, succeed)
        syncManager.removeStartedRequest(fileLocalPath)
        Timber.d("${request.operationType.name} finished for $fileLocalPath")
    }

    private fun canStart(): Boolean {
        val isSyncAllowed = CommonUtils.isThisSyncAllowed(account, request.syncedFileState.isMediaType)
        if (!isSyncAllowed){
            Timber.d("Sync of the file is not allowed anymore")
            return false
        }

        if (!isNetworkAvailable()) {
            Timber.d("No network available. can't run syncTask")
            return false
        }
        return true
    }


    private fun runUpload(syncWrapper: SyncWrapper): Boolean {
        @Suppress("DEPRECATION") val result = syncWrapper.remoteOperation?.execute(client)
        if (result == null) {
            Timber.d("Error: Upload result for $fileName is null")
            return false
        }

        val code = result.code
        Timber.d("Upload operation for $fileName result in: ${result.code}")

        if (code == RemoteOperationResult.ResultCode.UNKNOWN_ERROR || code == RemoteOperationResult.ResultCode.FORBIDDEN) {
            val operation = syncWrapper.remoteOperation as UploadFileOperation
            val rowAffected = DbHelper.forceFoldertoBeRescan(operation.syncedState.id, context)
            Timber.d("Force folder to be rescan next time (row affected) : $rowAffected")
            return false
        }

        if (isNetworkLost(code)) {
            syncManager.clearRequestQueue()
            Timber.d("Network has been lost. SyncRequest queue has been cleared")
            return false
        }
        return result.isSuccess
    }

    private fun runDownload(syncWrapper: SyncWrapper): Boolean {
        @Suppress("DEPRECATION") val result = syncWrapper.remoteOperation?.execute(client)
        if (result == null) {
            Timber.d("Error: Download result for $fileName is null")
            return false
        }

        val code = result.code
        Timber.d("Download operation for $fileName result in: ${result.code}")

        if (isNetworkLost(code)) {
            syncManager.clearRequestQueue()
            Timber.d("Network has been lost. SyncRequest queue has been cleared")
            return false
        }

        return result.isSuccess
    }

    private fun runSyncDisabling(): Boolean {
        val fileState = request.syncedFileState

        fileState.disableScanning()
        if (DbHelper.manageSyncedFileStateDB(fileState, "UPDATE", context) <= 0) {
            Timber.d("Failed to disable sync for $fileState.name from DB")
            return false
        }

        return true
    }

    private fun isNetworkLost(code: RemoteOperationResult.ResultCode): Boolean {
        return (code == WRONG_CONNECTION
                || code == NO_NETWORK_CONNECTION
                || code == HOST_NOT_AVAILABLE)
    }

    private fun updateFailureCounter(request: SyncRequest, success: Boolean) {
        val failedPrefs = FailedSyncPrefsManager.getInstance(context)
        val fileState = request.syncedFileState
        val fileStateId = fileState.id

        if (!success) {
            if (request.operationType == SyncRequest.Type.UPLOAD) {
                val filePath = fileState.localPath
                if (filePath.isEmpty()) return
                val file = File(filePath)
                if (file.length() >= UploadFileOperation.FILE_SIZE_FLOOR_FOR_CHUNKED) return
            }
            failedPrefs.saveFileSyncFailure(fileStateId)
            return
        }

        failedPrefs.removeDataForFile(fileStateId)
    }

    private fun isNetworkAvailable(): Boolean {
        val isMeteredNetworkAllowed = CommonUtils.isMeteredNetworkAllowed(account)
        return CommonUtils.haveNetworkConnection(context, isMeteredNetworkAllowed)
    }
}