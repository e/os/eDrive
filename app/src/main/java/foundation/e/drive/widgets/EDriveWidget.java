/*
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.widgets;

import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_ALIAS_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_EMAIL;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_NAME;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_TOTAL_QUOTA_KEY;
import static foundation.e.drive.utils.AppConstants.ACCOUNT_DATA_USED_QUOTA_KEY;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import foundation.e.drive.R;
import foundation.e.drive.account.AccountUtils;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.utils.AccessTokenProvider;
import timber.log.Timber;

/**
 * @author TheScarastic
 * Implementation of App Widget functionality.
 */
public class EDriveWidget extends AppWidgetProvider {

    public static final String WEBPAGE =
            "https://murena.com/ecloud-subscriptions/?username=%s&token=%s&current-quota=%s&from=wp";
    private static final String ADD_ACCOUNT_WEBPAGE = "https://murena.io/signup/e-email-invite";

    private static final String ACCOUNT_PROVIDER_EELO = "e.foundation.webdav.eelo";

    private static final String SHOW_ALIAS = "show_alias";
    private static final String HIDE_ALIAS = "hide_alias";
    private static final String COPY_ALIAS = "copy_alias";
    private static final String DARK_TEXT_KEY = "foundation.e.blisslauncher.WIDGET_OPTION_DARK_TEXT";
    private static boolean showAlias = false;
    // protected to avoid SyntheticAccessor
    protected static boolean isNetworkAvailable = false;
    private static boolean isDarkText = false;
    private final Calendar calender = Calendar.getInstance();
    private final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private RemoteViews views;
    private Account account = null;

    @NonNull
    public static String dataForWeb(@NonNull String bytes) {
        long convertedBytes = 0;
        try {
            convertedBytes =  Long.parseLong(bytes);
        } catch (NumberFormatException exception) {
            Timber.tag(EDriveWidget.class.getSimpleName()).i("Invalid bytes %s", bytes);
        }
        final String space = CommonUtils.humanReadableByteCountBin(convertedBytes);
        final String[] split = space.split(" ");
        return Math.round(Double.parseDouble(split[0])) + split[1];
    }

    @NonNull
    public static Intent buildIntent(@NonNull String name, @NonNull String extra) {
        final Intent intent = new Intent(name);
        if (!extra.isEmpty()) {
            intent.setData(Uri.parse(extra));
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static int convertIntoMB(@NonNull String quota) {
        int convertedData = 0;
        try {
            convertedData = (int) (Long.parseLong(quota) / (1024 * 1024));
        } catch (NumberFormatException ignore) {
        }
        return convertedData;
    }

    public synchronized void updateAppWidget(@NonNull final Context context) {
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        final ComponentName provider = new ComponentName(context, getClass());
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(provider);

        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    private void updateAppWidget(@NonNull final Context context, @NonNull final AppWidgetManager appWidgetManager,
                                final int appWidgetId) {
        final AccountManager accountManager = AccountManager.get(context);

        // Make Last check to see if account is available
        if (account == null) {
            account = CommonUtils.getAccount(context.getString(R.string.eelo_account_type),
                    accountManager);
        }

        if (!isAccountPresentInApp(context) || account == null) {
            noAccountView(context);
        } else {
            onAccountAvailable(context, accountManager);
        }

        if (views != null) {
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    private boolean isAccountPresentInApp(@NonNull Context context) {
        final SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        final String accountName = preferences.getString(AccountManager.KEY_ACCOUNT_NAME, "");

        return !accountName.trim().isEmpty();
    }

    @Override
    public void onEnabled(@NonNull Context context) {
        super.onEnabled(context);
        registerConnectivityCallback(context);
        updateAppWidget(context);
    }

    @Override
    public void onReceive(@NonNull final Context context, @NonNull Intent intent) {
        final String action = intent.getAction();
        if (action == null) return;
        switch (action) {
            case AppWidgetManager.ACTION_APPWIDGET_UPDATE:
            case AccountManager.LOGIN_ACCOUNTS_CHANGED_ACTION:
                updateAppWidget(context);
                break;
            case SHOW_ALIAS:
                showAlias = true;
                updateAppWidget(context);
                break;
            case HIDE_ALIAS:
                showAlias = false;
                updateAppWidget(context);
                break;
            case COPY_ALIAS:
                CommonUtils.copyToClipboard(intent.getData(), context,
                        context.getString(R.string.alias));
                break;
        }
        super.onReceive(context, intent);
    }

    private void noAccountView(@NonNull Context context) {
        if (!isNetworkAvailable) {
            noInternetView(context);
            return;
        }

        if (isDarkText) {
            views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget_login_light);
        } else {
            views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget_login);
        }

        final Intent accountIntent = buildIntent(Settings.ACTION_ADD_ACCOUNT, "")
                .putExtra(Settings.EXTRA_ACCOUNT_TYPES, new String[]{ACCOUNT_PROVIDER_EELO});
        final PendingIntent addAccountIntent = PendingIntent.getActivity(context, 0,
                accountIntent, PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.login, addAccountIntent);

        final PendingIntent pendingIntentNewAccount = PendingIntent.getActivity(context, 0,
                buildIntent(Intent.ACTION_VIEW, ADD_ACCOUNT_WEBPAGE), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.newAccount, pendingIntentNewAccount);
        views.setViewVisibility(R.id.button_container, View.VISIBLE);
        views.setTextViewText(R.id.summary, context.getString(R.string.login_summary));
    }

    private void noInternetView(@NonNull Context context) {
        if (!isNetworkAvailable) {
            if (isDarkText) {
                views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget_login_light);
            } else {
                views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget_login);
            }
            views.setViewVisibility(R.id.button_container, View.GONE);
            views.setTextViewText(R.id.summary, context.getString(R.string.no_internet_widget));
        }
    }

    private void onAccountAvailable(@NonNull Context context, @NonNull AccountManager accountManager) {
        final String usedQuota = accountManager.getUserData(account, ACCOUNT_DATA_USED_QUOTA_KEY);
        final String totalQuota = accountManager.getUserData(account, ACCOUNT_DATA_TOTAL_QUOTA_KEY);
        final String email = accountManager.getUserData(account, ACCOUNT_DATA_EMAIL);
        String name = accountManager.getUserData(account, ACCOUNT_DATA_NAME);
        final String token = AccessTokenProvider.getToken(accountManager, account);

        if (email == null || email.trim().isEmpty()) {
            noAccountView(context);
            return;
        }
        if (isDarkText) {
            views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget_light);
        } else {
            views = new RemoteViews(context.getPackageName(), R.layout.e_drive_widget);
        }
        views.setTextViewText(R.id.email, email);

        // For some reason if we cant get name use email as name
        if (name == null || name.isEmpty()) {
            name = email;
        }
        views.setTextViewText(R.id.name, name);

        views.setProgressBar(R.id.progress, convertIntoMB(totalQuota), convertIntoMB(usedQuota), false);

        String totalShownQuota = "?";
        String usedShownQuota = "?";

        Timber.tag(EDriveWidget.class.getSimpleName());
        //todo extract following duplicated code into a dedicated function
        try {
            final long totalQuotaLong = Long.parseLong(totalQuota);
            if (totalQuotaLong >= 0) {
                totalShownQuota = CommonUtils.humanReadableByteCountBin(totalQuotaLong);
            }
        } catch (NumberFormatException ignored) {
            Timber.d("Bad totalQuotaLong %s", totalQuota);
        }

        try {
            final long usedQuotaLong = Long.parseLong(usedQuota);
            if (usedQuotaLong >= 0) {
                usedShownQuota = CommonUtils.humanReadableByteCountBin(usedQuotaLong);
            }
        } catch (NumberFormatException ignore) {
            Timber.d("Bad usedQuotaLong %s", usedQuota);
        }

        views.setTextViewText(R.id.planName, context.getString(R.string.free_plan, totalShownQuota));

        final String premiumPlan = AccountUtils.getPremiumPlan(accountManager, account);
        if (premiumPlan != null) {
            views.setTextViewText(R.id.planName,
                        context.getString(R.string.premium_plan, premiumPlan));
        }

        views.setTextViewText(R.id.status, context.getString(R.string.progress_status,
                usedShownQuota, totalShownQuota));

        views.setTextViewText(R.id.sync, context.getString(R.string.last_synced,
                sdf.format(calender.getTime())));

        final String aliases = accountManager.getUserData(account, ACCOUNT_DATA_ALIAS_KEY);
        if (aliases == null || aliases.isEmpty()) {
            views.setViewVisibility(R.id.show_alias, View.GONE);
            views.setViewVisibility(R.id.alias1_container, View.GONE);
            views.setViewVisibility(R.id.hide_alias, View.GONE);
        } else {
            views.setTextViewText(R.id.alias1, context.getString(R.string.alias_dot) + aliases.split(",")[0]);
            views.setOnClickPendingIntent(R.id.show_alias, getPendingSelfIntent(context, SHOW_ALIAS,
                    null));
            views.setOnClickPendingIntent(R.id.hide_alias, getPendingSelfIntent(context, HIDE_ALIAS,
                    null));
            views.setOnClickPendingIntent(R.id.alias1_clipboard, getPendingSelfIntent(context, COPY_ALIAS,
                    String.valueOf(aliases.split(",")[0])));

            if (showAlias) {
                views.setViewVisibility(R.id.show_alias, View.GONE);
                views.setViewVisibility(R.id.hide_alias, View.VISIBLE);
                views.setViewVisibility(R.id.alias1_container, View.VISIBLE);
            } else {
                views.setViewVisibility(R.id.show_alias, View.VISIBLE);
                views.setViewVisibility(R.id.hide_alias, View.GONE);
                views.setViewVisibility(R.id.alias1_container, View.GONE);
            }
        }

        final PendingIntent pendingIntentSettings = PendingIntent.getActivity(context, 0,
                buildIntent(Settings.ACTION_SYNC_SETTINGS, ""), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.settings, pendingIntentSettings);

        final PendingIntent pendingIntentUpgrade = PendingIntent.getActivity(context, 0,
                buildIntent(Intent.ACTION_VIEW, String.format(WEBPAGE, email, token,
                        dataForWeb(totalQuota))), PendingIntent.FLAG_IMMUTABLE);
        views.setOnClickPendingIntent(R.id.upgrade, pendingIntentUpgrade);
    }

    @NonNull
    private PendingIntent getPendingSelfIntent(@NonNull Context context, @NonNull String action, @Nullable String data) {
        final Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        if (data != null) {
            intent.setData(Uri.parse(data));
        }
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);
    }

    private void registerConnectivityCallback(@NonNull final Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        cm.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(@NonNull Network network) {
                if (!isNetworkAvailable) {
                    isNetworkAvailable = true;
                    updateAppWidget(context);
                }
            }

            @Override
            public void onLost(@NonNull Network network) {
                if (isNetworkAvailable) {
                    isNetworkAvailable = false;
                    updateAppWidget(context);
                }
            }
        });
    }

    @Override
    public void onAppWidgetOptionsChanged(@NonNull Context context, @NonNull AppWidgetManager appWidgetManager, int appWidgetId, @NonNull Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        isDarkText = newOptions.getBoolean(DARK_TEXT_KEY);
        updateAppWidget(context);
    }
}