/*
 * Copyright (C) 2025 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.recovery

import android.content.Context
import foundation.e.drive.recovery.RecoveryPreferences.RecoveryStatus.RecoveryCompleted
import foundation.e.drive.recovery.RecoveryPreferences.RecoveryStatus.RecoveryNeeded

object RecoveryPreferences {

    private const val PREFERENCES_NAME = "edrive_recovery"
    private const val KEY_IS_DATA_ALREADY_RECOVERED = "is_data_already_recovered"

    @JvmStatic
    fun isRecoveryNeeded(context: Context): RecoveryStatus {
        val preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val isDataAlreadyRecovered = preferences.getBoolean(KEY_IS_DATA_ALREADY_RECOVERED, false)

        return if (isDataAlreadyRecovered) {
            RecoveryCompleted
        } else {
            RecoveryNeeded
        }
    }

    @JvmStatic
    fun updateRecoveryStatus(context: Context, status: RecoveryStatus) {
        val preferences =
            context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        when (status) {
            RecoveryCompleted -> {
                preferences.edit().putBoolean(KEY_IS_DATA_ALREADY_RECOVERED, true).apply()
            }

            RecoveryNeeded -> {
                preferences.edit().putBoolean(KEY_IS_DATA_ALREADY_RECOVERED, false).apply()
            }
        }
    }

    enum class RecoveryStatus {
        RecoveryNeeded,
        RecoveryCompleted
    }
}
