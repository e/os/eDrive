/*
 * Copyright (C) 2025 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.drive.recovery

import android.content.Context
import foundation.e.drive.R
import foundation.e.drive.account.AccountAdder
import foundation.e.drive.account.AccountRemover
import foundation.e.drive.account.AccountUtils.getAccount
import foundation.e.drive.recovery.RecoveryPreferences.RecoveryStatus.RecoveryCompleted
import foundation.e.drive.recovery.RecoveryPreferences.RecoveryStatus.RecoveryNeeded
import timber.log.Timber

class RecoveryManager(
    private val context: Context,
) {
    private val accountRemover = AccountRemover(context)
    private val accountAdder = AccountAdder(context)

    fun initiateRecovery() {
        if (isRecoveryNeeded()) {
            val (accountName, accountType) = getUserAccountInfo(context)

            if (accountName.isNotEmpty()) {
                Timber.i("Initiating data recovery for eDrive.")
                logoutUser()
                loginUser(accountName, accountType)
                updateRecoveryStatus(RecoveryCompleted)
            }
        }
    }

    private fun updateRecoveryStatus(status: RecoveryPreferences.RecoveryStatus) {
        RecoveryPreferences.updateRecoveryStatus(context, status)
        Timber.i("Updating eDrive recovery status to $status.")
    }

    private fun isRecoveryNeeded(): Boolean = when (RecoveryPreferences.isRecoveryNeeded(context)) {
        RecoveryNeeded -> true
        RecoveryCompleted -> false
    }

    private fun getUserAccountInfo(context: Context): Pair<String, String> {
        val accountName = getAccount(context.applicationContext)?.name ?: ""
        val accountType: String = context.getString(R.string.eelo_account_type)

        return Pair(accountName, accountType)
    }

    private fun loginUser(accountName: String, accountType: String) {
        accountAdder.addAccount(accountName, accountType)
        Timber.i("Added Account($accountName, $accountType) to eDrive.")
    }

    private fun logoutUser() {
        accountRemover.removeAccount()
        Timber.i("Account removed from eDrive.")
    }
}
