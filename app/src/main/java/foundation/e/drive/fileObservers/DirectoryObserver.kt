/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.fileObservers

import android.os.Build
import android.os.FileObserver
import androidx.annotation.RequiresApi
import foundation.e.drive.synchronization.StateMachine
import foundation.e.drive.synchronization.SyncState
import java.io.File

@RequiresApi(Build.VERSION_CODES.Q)
class DirectoryObserver(
    private val dirPath: String,
    private val listener: FileEventListener
) : FileObserver(File(dirPath), EVENT_MASKS) {

    companion object {
        const val EVENT_MASKS = (CLOSE_WRITE or CREATE
                or MOVED_FROM or MOVED_TO or MOVE_SELF
                or DELETE_SELF or DELETE)
    }

    override fun onEvent(event: Int, path: String?) {
        val syncState = StateMachine.currentState
        if (syncState != SyncState.LISTENING_FILES) {
            return
        }

        val file = if (path == null) File(dirPath) else File(dirPath, path)

        // to retrieve actual event code, we need to apply conjunction operation with the all event code
        listener.notify(event and ALL_EVENTS, file, dirPath)
    }
}
