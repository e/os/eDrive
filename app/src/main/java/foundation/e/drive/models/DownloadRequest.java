/*
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.drive.models;

import androidx.annotation.NonNull;

import com.owncloud.android.lib.resources.files.model.RemoteFile;

/**
 * @author vincent Bourgmayer
 */
public class DownloadRequest extends SyncRequest {
    private final RemoteFile remoteFile;

    public DownloadRequest (@NonNull RemoteFile remoteFile, @NonNull SyncedFileState syncedFileState) {
        super(syncedFileState, Type.DOWNLOAD);
        this.remoteFile = remoteFile;
    }

    @NonNull
    public RemoteFile getRemoteFile() {
        return remoteFile;
    }
}
