/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Vincent Bourgmayer
 * This class encapsulates the representation of a synced or syncable folder.
 * It stores data about remote and local real folder.
 */

public class SyncedFolder implements Parcelable {
    private int id;
    private String libelle;
    private String localFolder;
    private String remoteFolder;
    private String lastEtag;
    private long lastModified =0L;
    private boolean scanLocal;
    private boolean scanRemote;
    private boolean enabled;
    private boolean isMediaType; //True if its a media's folder, false if it is a settings's folder
    private boolean toSync; //NO NEED TO PERSIST

    /**
     * Constructor for SyncedFolder object. Enabled, scanRemote and scanLocal are true by default.
     * @param libelle libelle (=categorie) of Synced folder
     * @param localFolder  path of the folder on device
     * @param remoteFolder path of the folder on server
     * @param isMediaType If this folder is a media's folder (true) or a settings's folder (false)
     */
    public SyncedFolder(@Nullable String libelle, @NonNull String localFolder, @NonNull String remoteFolder, boolean isMediaType){
        this.enabled = true;
        this.scanLocal = true;
        this.scanRemote = true;
        this.libelle = libelle;
        this.localFolder = localFolder;
        this.remoteFolder = remoteFolder;
        this.isMediaType = isMediaType;
    }

    /**
     * Constructor for SyncedFolder object. Enabled is true by default.
     * @param libelle libelle (=categorie) of Synced folder
     * @param localFolder path of the folder on device
     * @param remoteFolder path of the folder on server
     * @param scanLocal Allow to be scanned by LocalObserverService if set to true
     * @param scanRemote Allow to be scanned by remoteObserverService if set to true
     * @param enabled If this folder is available for synchronisation
     * @param isMediaType If this folder is a media's folder (true) or a settings's folder (false)
     */
    public SyncedFolder(@Nullable String libelle, @NonNull String localFolder, @NonNull String remoteFolder, boolean scanLocal, boolean scanRemote, boolean enabled, boolean isMediaType){
        this.libelle = libelle;
        this.localFolder = localFolder;
        this.remoteFolder = remoteFolder;
        this.scanLocal = scanLocal;
        this.scanRemote = scanRemote;
        this.enabled = enabled;
        this.isMediaType = isMediaType;
    }

    /**
     * Constructor from a parcel
     * @param in The parcel containing data to build the object
     */
     // protected to avoid SyntheticAccessor
     protected SyncedFolder(@NonNull Parcel in){
        this.id = in.readInt();
        this.libelle = in.readString();
        this.localFolder = in.readString();
        this.remoteFolder = in.readString();
        this.scanLocal = in.readInt() == 1 ;
        this.scanRemote = in.readInt() == 1 ;
        this.enabled = in.readInt() == 1 ;
        this.lastEtag = in.readString();
        this.lastModified = in.readLong();
        this.isMediaType = in.readInt() == 1;
        this.toSync = in.readInt() == 1;
    }

    /**
     * Create a SyncedFolder as a child of an other SyncedFolder
     * @param parent The parent SyncedFolder
     * @param suffixPath The part of the path that its parent doesn't have. Must not start by '\'
     * @param lastModified LastModified value of the current local folder (must be 0L if not known)
     * @param lastEtag Last etag of remote folder (must be empty string if not known)
     */
    public SyncedFolder(@NonNull SyncedFolder parent, @NonNull String suffixPath, long lastModified, @Nullable String lastEtag){
        this.id = -1;
        this.libelle = parent.libelle;
        this.localFolder = parent.localFolder+suffixPath;
        this.remoteFolder = parent.remoteFolder+suffixPath;
        this.scanLocal = parent.scanLocal;
        this.scanRemote = parent.scanRemote;
        this.lastModified = lastModified;
        this.lastEtag = lastEtag;
        this.enabled = parent.enabled;
        this.isMediaType = parent.isMediaType;
    }



    public static final Creator<SyncedFolder> CREATOR = new Creator<SyncedFolder>() {
        @Override
        public SyncedFolder createFromParcel(Parcel in) {
            return new SyncedFolder(in);
        }

        @Override
        public SyncedFolder[] newArray(int size) {
            return new SyncedFolder[size];
        }
    };

    @Nullable
    public String getLibelle() {
        return libelle;
    }

    @NonNull
    public String getLocalFolder() {
        return localFolder;
    }

    @NonNull
    public String getRemoteFolder() {
        return remoteFolder;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @NonNull
    public SyncedFolder setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public SyncedFolder setId(int id) {
        this.id = id;
        return this;
    }

    @Nullable
    public String getLastEtag() {
        return lastEtag;
    }

    @NonNull
    public SyncedFolder setLastEtag(@Nullable String lastEtag) {
        this.lastEtag = lastEtag;
        return this;
    }

    public long getLastModified() {
        return lastModified;
    }

    @NonNull
    public SyncedFolder setLastModified(long lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    /**
     * Get boolean to know if it should be synced or not.
     * This attribute isn't persist in DB!
     * @return true if it is sync
     */
    public boolean isToSync() {
        return toSync;
    }

    /**
     * Use to known if this syncedFolder should be synced this time
     * This attribute isn't persist!
     * @param toSync True to sync files in this syncedFolder
     * @return calling instance
     */
    @NonNull
    public SyncedFolder setToSync(boolean toSync) {
        this.toSync = toSync;
        return this;
    }

    public boolean isScanLocal() {
        return scanLocal;
    }

    public boolean isScanRemote() {
        return scanRemote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Give the type of synchronisable element it is
     * @return true if it's a folder with medias. False if it's a folder with settings
     */
    public boolean isMediaType() {
        return isMediaType;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.libelle);
        dest.writeString(this.localFolder);
        dest.writeString(this.remoteFolder);
        dest.writeInt(this.scanLocal ? 1 : 0);
        dest.writeInt(this.scanRemote ? 1 : 0);
        dest.writeInt(this.enabled ? 1 : 0);
        dest.writeString(this.lastEtag);
        dest.writeLong(this.lastModified);
        dest.writeInt(this.isMediaType ? 1 : 0);
        dest.writeInt(this.toSync? 1: 0);
    }

    @Override
    public String toString(){
        return "SyncedFolder :\nid : "+this.id
                +"\n libelle : "+this.libelle
                +"\n local folder : "+this.localFolder
                +"\n remote folder : "+this.remoteFolder
                +"\n scan Local : "+this.scanLocal
                +"\n scan remote : "+this.scanRemote
                +"\n enabled : "+this.enabled
                +"\n last etag : "+this.lastEtag
                +"\n last modified : "+this.lastModified
                +"\n toSync : "+this.toSync
                +"\n isMediaType: "+this.isMediaType;
    }
}
