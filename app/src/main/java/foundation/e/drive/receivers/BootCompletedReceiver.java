/*
 * Copyright © MURENA SAS 2022-2024.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.receivers;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;

import androidx.annotation.NonNull;
import androidx.work.WorkManager;

import foundation.e.drive.BuildConfig;
import foundation.e.drive.database.DbHelper;
import foundation.e.drive.synchronization.SyncProxy;
import foundation.e.drive.utils.AppConstants;
import foundation.e.drive.utils.CommonUtils;
import foundation.e.drive.work.WorkLauncher;
import foundation.e.drive.account.AccountUtils;
import timber.log.Timber;

/**
 * @author Abhishek Aggarwal
 * @author vincent Bourgmayer
 */
public class BootCompletedReceiver extends BroadcastReceiver {
    private static final String DATE_SYSTEM_PROPERTY = "ro.build.date";
    private static final String PREF_VERSION_CODE = "VERSION_CODE";
    private static final String OLD_SETUP_COMPLETED_PREF_KEY ="initService_has_run";

    private static final int VERSION_CODE_FOR_UPDATE_1 = 1002000;
    private static final int VERSION_CODE_FOR_UPDATE_2 = 1003017;



    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        final String action = intent.getAction();
        Timber.v("onReceive(...)");

        if (!Intent.ACTION_BOOT_COMPLETED.equals(action)
            || !AccountUtils.isAccountAvailable(context)) {
            return;
        }

        final SharedPreferences prefs = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        final WorkLauncher workLauncher = WorkLauncher.getInstance(context);

        migrateSetupCompletedPrefsKey(prefs);
        if (!isSetupCompleted(prefs)){
            workLauncher.enqueueSetupWorkers(context);
            return;
        }

        /*
         * App was persistent so can only be updated (by replacement) on OS update
         * note: still needs the below check and call when migrating for persistent eDrive to not persistent eDrive.
         * But on "not persistent eDrive" update "to not persistent eDrive" update+1, this won't be required again
         */
        final String currentDateProp = CommonUtils.getProp(DATE_SYSTEM_PROPERTY);
        if (isOsUpdated(prefs, currentDateProp)) {
            handleOsUpdate(context);
        }

        final int lastSavedVersionCode = prefs.getInt(PREF_VERSION_CODE, VERSION_CODE_FOR_UPDATE_1);
        if (isEdriveUpdated(lastSavedVersionCode)) {
            onEdriveUpdate(lastSavedVersionCode, context);
            prefs.edit().putInt(PREF_VERSION_CODE, BuildConfig.VERSION_CODE).apply();
        }

        SyncProxy.INSTANCE.startListeningFiles((Application) context.getApplicationContext());

        workLauncher.enqueuePeriodicFullScan();
        workLauncher.enqueuePeriodicUserInfoFetching();
    }

    private void migrateSetupCompletedPrefsKey(@NonNull SharedPreferences prefs) {
        if (prefs.getBoolean(OLD_SETUP_COMPLETED_PREF_KEY, false)) {
            Timber.i("Update setup complete preferences");
            prefs
                .edit()
                .remove(OLD_SETUP_COMPLETED_PREF_KEY)
                .putBoolean(AppConstants.SETUP_COMPLETED, true)
                .apply();
        }
    }

    /** @noinspection SpellCheckingInspection*/
    private boolean isEdriveUpdated(int oldVersionCode) {
        Timber.d("isEdriveUpdated (%s > %s) ?", BuildConfig.VERSION_CODE, oldVersionCode);
        return BuildConfig.VERSION_CODE > oldVersionCode;
    }

    private void onEdriveUpdate(int oldVersionCode, @NonNull Context context) {
        if (oldVersionCode <= VERSION_CODE_FOR_UPDATE_1) {
            try {
                DbHelper.cleanSyncedFileStateTableAfterUpdate(context);
            } catch (SQLiteException exception) {
                Timber.e(exception);
            }
        }

        if (oldVersionCode <= VERSION_CODE_FOR_UPDATE_2) {
            Timber.d("Triggered the update 2 from: %s", oldVersionCode);
            final WorkManager workManager= WorkManager.getInstance(context);
            workManager.cancelAllWork();

            final WorkLauncher workLauncher = WorkLauncher.getInstance(context);
            workLauncher.enqueuePeriodicFullScan();
            workLauncher.enqueuePeriodicUserInfoFetching();
        }
    }

    private boolean isOsUpdated(@NonNull SharedPreferences prefs, @NonNull String currentDateProp) {
        final String lastKnownDateProp = prefs.getString(DATE_SYSTEM_PROPERTY, "");
        return !currentDateProp.equals(lastKnownDateProp);
    }

    /**
     * Force reinitialization, upgrade of DB in case of OS update
     * @param context Context used to start InitializationService
     */
    private void handleOsUpdate(@NonNull Context context) {
        final DbHelper dbHelper = new DbHelper(context);
        dbHelper.getWritableDatabase().close(); //force DB update
    }

    private boolean isSetupCompleted(@NonNull SharedPreferences prefs) {
        return prefs.getBoolean(AppConstants.SETUP_COMPLETED, false);
    }
}
