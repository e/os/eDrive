/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © ECORP SAS 2022.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import android.provider.BaseColumns;

/**
* @author Vincent Bourgmayer
* Offer structure of SyncedFileState's Table
*/
class SyncedFileStateContract implements BaseColumns{

   /** Table structure **/
    static final String TABLE_NAME ="synced_file_state";
    static final String FILE_NAME ="file_name";
    static final String LOCAL_PATH ="local_path";
    static final String REMOTE_PATH = "remote_path";
    static final String LAST_ETAG = "last_etag";
    static final String LOCAL_LAST_MODIFIED = "local_last_modified";
    static final String SYNCEDFOLDER_ID = "synced_folder_id";
    static final String IS_MEDIA_TYPE = "is_media_type";
    static final String SCANNABLE = "scannable";

    static final String SQL_CREATE_TABLE_SYNCEDFILESTATE =
         "CREATE TABLE "+TABLE_NAME+" ( "
         +_ID+" INTEGER PRIMARY KEY, "
         +FILE_NAME+" TEXT, "
         +LOCAL_PATH+" TEXT, "
         +REMOTE_PATH+" TEXT, "
         +LAST_ETAG+" TEXT, "
         +LOCAL_LAST_MODIFIED+" INTEGER, "
         + SYNCEDFOLDER_ID +" INTEGER, "
         +IS_MEDIA_TYPE+" BOOLEAN,"
         +SCANNABLE+" INTEGER, "
         +"CONSTRAINT synced_unicity_constraint UNIQUE ("
         +FILE_NAME+", "
         +LOCAL_PATH+", "
         +REMOTE_PATH
         +"))";

    static final String SQL_DELETE_TABLE_SYNCEDFILESTATE = " DROP TABLE IF EXISTS "
           + TABLE_NAME;

    //Update for version 18 and lower
    static final String UPDATE_TABLE_TO_VERSION_19 = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+IS_MEDIA_TYPE+" BOOLEAN;";
    static final String UPDATE_MEDIA_DATA_TO_VERSION_19 = "UPDATE "+TABLE_NAME+
            " SET "+IS_MEDIA_TYPE+" = 1 WHERE "+
            REMOTE_PATH+" LIKE \"/Photos/%\" OR "+
            REMOTE_PATH+" LIKE \"/Movies/%\" OR "+
            REMOTE_PATH+" LIKE \"/Pictures/%\" OR "+
            REMOTE_PATH+" LIKE \"/Music/%\" OR "+
            REMOTE_PATH+" LIKE \"/Ringtones/%\" OR "+
            REMOTE_PATH+" LIKE \"/Documents/%\" OR "+
            REMOTE_PATH+" LIKE \"/Podcasts/%\";";

    static final String UPDATE_SETTINGS_DATA_TO_VERSION_19 = "UPDATE "+TABLE_NAME+
            " SET "+IS_MEDIA_TYPE+" = 0 WHERE "+REMOTE_PATH+" LIKE \"/Devices/%\";";

    //update for version 19 and lower
    static final String UPDATE_TABLE_TO_VERSION_20 = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+SCANNABLE+" INTEGER;";
    static final String UPDATE_MEDIA_DATA_TO_VERSION_20 = "UPDATE "+TABLE_NAME+" SET "+SCANNABLE+" = 3 WHERE "
            + IS_MEDIA_TYPE+" = 1 ;" ;
    static final String UPDATE_SETTINGS_DATA_TO_VERSION_20 = "UPDATE "+TABLE_NAME+" SET "+SCANNABLE+" = 2 WHERE "
         + IS_MEDIA_TYPE+" = 0 ;" ;

}
