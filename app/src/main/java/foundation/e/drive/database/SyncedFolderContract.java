/*
 * Copyright © CLEUS SAS 2018-2019.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import android.provider.BaseColumns;

/**
 * @author Vincent Bourgmayer
 * Contains structure of SyncedFolder's table and query to create and delete it
 * source: https://developer.android.com/training/data-storage/sqlite.html#java
 */
final class SyncedFolderContract implements BaseColumns{

    /** Table Structure **/
    static final String TABLE_NAME ="synced_folder";
    static final String CATEGORIE_LABEL ="categorie_label";
    static final String LOCAL_PATH ="local_path";
    static final String REMOTE_PATH ="remote_path";
    static final String LAST_ETAG ="last_etag";
    static final String LOCAL_LAST_MODIFIED ="last_modified";
    static final String SCANLOCAL ="scan_local";
    static final String SCANREMOTE ="scan_remote";
    static final String ENABLED ="enabled";
    static final String IS_MEDIA_TYPE = "is_media_type";

    static final String SQL_CREATE_TABLE_SYNCEDFOLDER =
            "CREATE TABLE "+TABLE_NAME+" ( "
            +_ID+" INTEGER PRIMARY KEY, "
            +CATEGORIE_LABEL+" TEXT, "
            +LOCAL_PATH+" TEXT unique, "
            +REMOTE_PATH+" TEXT unique, "
            +LAST_ETAG+" TEXT, "
            +LOCAL_LAST_MODIFIED+" INTEGER, "
            +SCANLOCAL+" BOOLEAN, "
            +SCANREMOTE+" BOOLEAN, "
            +ENABLED+" BOOLEAN, "
            +IS_MEDIA_TYPE+" BOOLEAN ) ";

    static final String SQL_DELETE_TABLE_SYNCEDFOLDER = " DROP TABLE IF EXISTS "
            + TABLE_NAME;

    static final String UPDATE_TABLE_TO_VERSION_19 = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+IS_MEDIA_TYPE+" BOOLEAN;";

    static final String UPDATE_MEDIA_DATA_TO_VERSION_19 = "UPDATE "+TABLE_NAME+
            " SET "+IS_MEDIA_TYPE+" = 1 WHERE "+
            REMOTE_PATH+" LIKE \"/Photos/%\" OR "+
            REMOTE_PATH+" LIKE \"/Movies/%\" OR "+
            REMOTE_PATH+" LIKE \"/Pictures/%\" OR "+
            REMOTE_PATH+" LIKE \"/Music/%\" OR "+
            REMOTE_PATH+" LIKE \"/Ringtones/%\" OR "+
            REMOTE_PATH+" LIKE \"/Documents/%\" OR "+
            REMOTE_PATH+" LIKE \"/Podcasts/%\";";

    static final String UPDATE_SETTINGS_DATA_TO_VERSION_19 = "UPDATE "+TABLE_NAME+
            " SET "+IS_MEDIA_TYPE+" = 0 WHERE "+REMOTE_PATH+" LIKE \"/Devices/%\";";
}
