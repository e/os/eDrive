/*
 * Copyright © CLEUS SAS 2018-2019.
 * Copyright © MURENA SAS 2022-2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package foundation.e.drive.database;

import static foundation.e.drive.database.SyncedFileStateContract.FILE_NAME;
import static foundation.e.drive.database.SyncedFileStateContract.IS_MEDIA_TYPE;
import static foundation.e.drive.database.SyncedFileStateContract.LAST_ETAG;
import static foundation.e.drive.database.SyncedFileStateContract.LOCAL_LAST_MODIFIED;
import static foundation.e.drive.database.SyncedFileStateContract.LOCAL_PATH;
import static foundation.e.drive.database.SyncedFileStateContract.REMOTE_PATH;
import static foundation.e.drive.database.SyncedFileStateContract.SCANNABLE;
import static foundation.e.drive.database.SyncedFileStateContract.SYNCEDFOLDER_ID;
import static foundation.e.drive.database.SyncedFileStateContract.TABLE_NAME;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import foundation.e.drive.models.SyncedFileState;
import timber.log.Timber;

/**
 * @author Vincent Bourgmayer
 * Offers Query to CRUD operation for SyncedFIleState Object
 */
/* package */ class SyncedFileStateDAO {
    private SQLiteDatabase mDB;
    private final DbHelper mHelper;
    private static final String[] allColumns = new String[] { SyncedFileStateContract._ID,
            FILE_NAME,
            LOCAL_PATH,
            REMOTE_PATH,
            LAST_ETAG,
            LOCAL_LAST_MODIFIED,
            SYNCEDFOLDER_ID,
            IS_MEDIA_TYPE,
            SCANNABLE
    };


    /* package */ SyncedFileStateDAO(@NonNull Context context){
        mHelper = new DbHelper(context);
        Timber.tag(SyncedFileStateDAO.class.getSimpleName());
    }

    /**
    * Open Database connexion
    * @param writeMod if true, open DB in write mod, else open it in read mod
    * @throws SQLException throw an SQL Exception
    */
    /* package */ void open(Boolean writeMod ) throws SQLException {
        mDB = ( writeMod ) ? mHelper.getWritableDatabase(): mHelper.getReadableDatabase();
    }

    /**
     * Close DB connexion.
     */
    /* package */ void close() {
        mHelper.close();
    }

    /**
     * Convert a SyncedFileState into a ContentValues object
     * @param syncedFileState syncedFileState to convert
     * @return a ContentValues object
     */
    private ContentValues toContentValues(SyncedFileState syncedFileState) {
        final ContentValues values = new ContentValues();
        values.put( FILE_NAME, syncedFileState.getName() );
        values.put( LOCAL_PATH, syncedFileState.getLocalPath() );
        values.put( REMOTE_PATH, syncedFileState.getRemotePath() );
        values.put( LAST_ETAG, syncedFileState.getLastEtag() );
        values.put( LOCAL_LAST_MODIFIED, syncedFileState.getLastModified() );
        values.put( SYNCEDFOLDER_ID, syncedFileState.getSyncedFolderId() );
        values.put( IS_MEDIA_TYPE, syncedFileState.isMediaType() ? 1 : 0 );
        values.put( SCANNABLE, syncedFileState.getScanScope());
        return values;
    }

    /**
    * Insert new SyncedFileState
    * @param syncedFileState element to register
    * @return id of the last added element
    */
    /* package */ long insert(SyncedFileState syncedFileState){
        return mDB.insertWithOnConflict(SyncedFileStateContract.TABLE_NAME,
                null,
                toContentValues(syncedFileState),
                SQLiteDatabase.CONFLICT_IGNORE);
    }

    /**
     * Delete SyncedFileState
     * @param id syncedFile's id in DB
     */
    /* package */ int delete(long id) {
        return mDB.delete(SyncedFileStateContract.TABLE_NAME, SyncedFileStateContract._ID
                + " = " + id, null);
    }

    /**
     * Remove SyncedFileState for hidden files (starting with '.')
     * @return number of deleted input
     */
    /* package */ int deleteHiddenFileStates() {
        return mDB.delete(TABLE_NAME, FILE_NAME + " LIKE ?", new String[]{".%"});
    }

    /**
    * Update a specific syncedFile
    * @param syncedFileState SyncedFileState to update
    * @return number of row affected
    */
    /* package */ int update(SyncedFileState syncedFileState){
        return  mDB.update(TABLE_NAME,
                    toContentValues(syncedFileState),
                    SyncedFileStateContract._ID+" = "+syncedFileState.getId(),
                    null);
    }


    /**
     * Count number of SyncedFileState's entry with an empty eTAG for the given syncedFolder.
     * ONLY list file that can be scan on device side
     * @param syncedFolderId ID of the given synced folder
     * @return number of file waiting to be uploaded
     * @throws SQLiteDoneException SQL return 0 rows
     */
    /* package */ long countFileWaitingForUploadForSyncedFolder(long syncedFolderId) throws SQLiteDoneException {
        final String query = "SELECT COUNT(*) FROM "
            + TABLE_NAME
            + " WHERE " + SCANNABLE + " >= 2"
            + " AND " + LAST_ETAG + " = ''"
            + " AND " + SYNCEDFOLDER_ID + " = "+syncedFolderId;

        final SQLiteStatement statement = mDB.compileStatement(query);
        return statement.simpleQueryForLong();
    }

    /**
     * Count number of syncedFileState's entry with an eTag but no value for local last modified
     * Only file that can be scan on cloud are listed
     * @param syncedFolderId The id of the syncedFolder
     * @return number of file waiting to be downloaded
     * @throws SQLiteDoneException SQL return 0 rows
     */
    /* package */ long countFileWaitingForDownloadForSyncedFolder(int syncedFolderId) throws SQLiteDoneException{
        final String query = "SELECT COUNT(*) FROM "
                + TABLE_NAME
                + " WHERE " + SCANNABLE + " IN (1,3)"
                + " AND " + LOCAL_LAST_MODIFIED + " = 0"
                + " AND length(" + LAST_ETAG + ") > 0"
                + " AND " + SYNCEDFOLDER_ID + " = "+syncedFolderId;

        final SQLiteStatement statement = mDB.compileStatement(query);
        return statement.simpleQueryForLong();
    }

    /**
     * Fetch every syncedFileState for subfile of directories identified by syncedFolder's id.
     *
     * @param syncedFolderIds List<Long> of SyncedFolderIds
     * @return List<SyncedFileState> List of SyncedFileState filtered on syncedFolder ID. The result is an empty list
     * if list of syncedFolderIds is null or empty
     */
    /* package */ List<SyncedFileState> getBySyncedFolderIds(List<Long> syncedFolderIds) {
        final List<SyncedFileState> result = new ArrayList<>();
        if (syncedFolderIds == null || syncedFolderIds.isEmpty()) {
            Timber.d("getBySyncedFolderIds(): SyncedFOlderIds is empty or null");
            return result;
        }

        final List<String> matcherList = new ArrayList<>(); //list of "?" to be replaced by value by SQliteDatabase.query() call

        final String[] whereValue = new String[syncedFolderIds.size()];
        for (int i = 0; i < syncedFolderIds.size(); i++) {
            matcherList.add("?");
            whereValue[i] = syncedFolderIds.get(i).toString();
        }

        final String whereClause = SYNCEDFOLDER_ID +
                " IN " +
                matcherList.toString()
                        .replace("[", "(")
                        .replace("]", ")");

        final Cursor cursor = mDB.query(TABLE_NAME, allColumns, whereClause, whereValue, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast() ) {
            result.add( cursorToSyncedFileState(cursor) );
            cursor.moveToNext();
        }
        cursor.close();
        return result;
    }

    /**
    * Fetch a SyncedFileState by its localPath
    * @param path local path or remote path
    * @return SyncedFileState obtain by the query or null if none has been found
    */
    /* package */ SyncedFileState getByPath(String path, boolean isLocalPath) {
        final String whereClause = (isLocalPath ? LOCAL_PATH : REMOTE_PATH) + " LIKE ?";
        final String[] whereValue = new String[] {path};
        final Cursor cursor = mDB.query(TABLE_NAME, allColumns, whereClause, whereValue, null, null, null);
        cursor.moveToFirst();
        SyncedFileState syncedFileState = null;
        if (!cursor.isAfterLast()) {
            syncedFileState = cursorToSyncedFileState(cursor);
        }
        cursor.close();
        return syncedFileState;
    }

    /**
     * Create SyncedFileState from cursor
     * @param cursor Cursor containing data to build SyncedFileState
     * @return SyncedFileState instance
     */
    private SyncedFileState cursorToSyncedFileState(Cursor cursor) {
        return new SyncedFileState(
                cursor.getInt(0), //id
                cursor.getString(1 ),//file name
                cursor.getString(2 ),//local path
                cursor.getString(3 ),//remote path
                cursor.getString(4 ),// last Etag
                cursor.getLong(5 ),//Local mTime
                cursor.getLong(6 ), //SyncedFolderID
                (cursor.getInt(7) == 1), //is Media Type
                cursor.getInt(8) //scanScope
        );
    }
}