image: "registry.gitlab.e.foundation/e/os/docker-android-apps-cicd:latest"

variables:
  SENTRY_DSN: "${SENTRY_DSN}"
  APK_PATH: "app/build/outputs/apk/release"
  UNSIGNED_APK: "eDrive-release-unsigned.apk"
  COMMUNITY_APK: "eDrive-community.apk"
  OFFICIAL_APK: "eDrive-official.apk"
  TEST_APK: "eDrive-test.apk"

stages:
  - build
  - test
  - gitlab_release

before_script:
  - export GRADLE_USER_HOME=$(pwd)/.gradle
  - chmod +x ./gradlew

cache:
  key: ${CI_PROJECT_ID}
  paths:
    - .gradle/


lint:
  stage: build
  script:
    - ./gradlew lintRelease
    - ./gradlew detekt
  artifacts:
    paths:
      - build/reports/detekt/

# |===========|
# | Build APK |
# |===========| 
build:
  stage: build
  script:
    - ./gradlew assemble
    - cd app/build/outputs/apk/
    - |
      if [[ ! -d "release" ]]; then
        echo "$APK_PATH does not exist."
        exit 1
      fi
      cd "release"
    - unsigned_build=$(ls *.apk | grep "unsigned")
    - cp $unsigned_build $UNSIGNED_APK
  artifacts:
    paths:
      - app/build/outputs/apk/
    expire_in: 4 weeks

test:
  allow_failure: true
  stage: test
  script:
    - ls /usr/lib/jvm/
    - ./gradlew test -PtestAccountName="$testAccountName" -PtestAccountPwd="$testAccountPwd" -PtestServerUrl="$testServerUrl"
  artifacts:
    when: always
    paths:
      - app/build/test-results/*/TEST-*.xml
      - app/build/reports/tests/*
    reports:
      junit: app/build/test-results/*/TEST-*.xml


#  Below job makes eDrive updatable through app lounge 

init_submodules:
  stage: gitlab_release
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
  script:
    - git clone https://gitlab.e.foundation/e/os/system-apps-update-info.git systemAppsUpdateInfo
  artifacts:
    paths:
      - systemAppsUpdateInfo/scripts/


generate-apks:
  stage: gitlab_release
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"' 
      when: on_success
  needs:
    - init_submodules
    - build
  dependencies:
    - init_submodules
    - build
  script:
    - |
      ./systemAppsUpdateInfo/scripts/generate-apks.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK" "$TEST_APK"
  artifacts:
    paths:
      - $APK_PATH/$UNSIGNED_APK
      - $APK_PATH/$COMMUNITY_APK
      - $APK_PATH/$OFFICIAL_APK
      - $APK_PATH/$TEST_APK

create-json-files:
  stage: gitlab_release
  dependencies:
    - init_submodules
    - generate-apks
  needs:
    - init_submodules
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: on_success
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-json-files.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK" "$TEST_APK"
  artifacts:
    paths:
      - community.json
      - official.json
      - test.json

create-test-release:
  stage: gitlab_release
  dependencies:
    - init_submodules
  needs:
    - init_submodules
    - create-json-files
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: manual
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-test-release.sh \
      "$APK_PATH" "$TEST_APK"
  allow_failure: true

create-release:
  stage: gitlab_release
  dependencies:
    - init_submodules
  needs:
    - init_submodules
    - create-json-files
    - generate-apks
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED == "true"'
      when: manual
  script:
    - |
      ./systemAppsUpdateInfo/scripts/create-release.sh \
      "$APK_PATH" "$UNSIGNED_APK" "$COMMUNITY_APK" "$OFFICIAL_APK"
  allow_failure: true
  